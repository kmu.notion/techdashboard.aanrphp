@include('admin modals.userModals')

<div class="card shadow">
    <div class="card-header">
        <i class="fas fa-users" style="font-size:25px;"></i>
        <span style="font-size:22px;">Users 
            <button type="button" class="btn btn-success text-right float-right" data-toggle="modal" data-target="#createUserModal"><i class="fas fa-user-plus"></i> Create User</button> 
        </span>
    </div>
    <div class="card-body">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>User Level</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach(App\User::all() as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <form action="{{ route('changeUserLevel', $user->id) }}" method="POST" id="roles-form-{{$user->id}}">
                                <select name="user_level" class="form-control" onchange="document.getElementById('roles-form-{{$user->id}}').submit()">
                                    <option value="5" {{$user->user_level == 5 ? 'selected' : ''}}>Superadmin (5)</option>
                                    <option value="4" {{$user->user_level == 4 ? 'selected' : ''}}>User from TTPD (4)</option>
                                    <option value="3" {{$user->user_level == 3 ? 'selected' : ''}}>User from PCAARRD (3)</option>
                                    <option value="2" {{$user->user_level == 2 ? 'selected' : ''}}>Standard User (2)</option>
                                </select>
                                {{ csrf_field() }} 
                            </form>
                        </td>
                        <td>
                            <button class="btn btn-danger px-2 py-1" data-toggle="modal" data-target="#deleteUserModal-{{$user->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>