@include('admin modals.agencyModals')

<div class="row mt-3">
    <a href="{{ route('pages.getAdmin', ['showAgency' => 'all'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showAgency=='all' || !request()->showAgency ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            All Agencies <span class="badge badge-warning">{{App\Agency::all()->where('approved', '=', '2')->count()}}</span>
        </button>
    </a>
    <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
    <a href="{{ route('pages.getAdmin', ['showAgency' => 'user'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showAgency=='user' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            My Items <span class="badge badge-warning">{{App\Agency::all()->where('user_id','=',auth()->user()->id)->count()}}</span>
        </button>
    </a>
    @if(auth()->user()->user_level == 5)
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showAgency' => 'approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showAgency=='approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Awaiting Approval <span class="badge badge-{{$agencyApprovalCount > 0 ? 'danger' : 'warning'}}">{{$agencyApprovalCount}}</span>
            </button>
        </a>
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showAgency' => 'edit-approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showAgency=='edit-approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Agency Edit Requests <span class="badge badge-{{$agencyEditApprovalCount > 0 ? 'danger' : 'warning'}}">{{$agencyEditApprovalCount}}</span>
            </button>
        </a>
    @endif
</div>
@if(request()->showAgency == "user")
<div class="card shadow">
    <div class="card-header">
        <h2>My Agencies
            <a type="button" class="btn btn-success text-right float-right" data-toggle="modal" data-target="#createAgencyModal"><i class="fas fa-plus"></i> Add</a> 
        </h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="5%">Agency ID</th>
                    <th width="10%">Date Created</th>
                    <th width="60%">Name</th>
                    <th class="text-center" width="10%">Status</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\Agency::where('user_id', '=', Auth::user()->id)->get() as $agency)
                    <tr>
                        <td>{{ $agency->id }}</td>
                        <td>{{ Carbon\Carbon::parse($agency->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $agency->name }}</td>
                        <td>
                            <div class="d-none d-sm-block status-pill {{$agency->approved == 2 ? 'approved-pill' : ''}} {{$agency->approved == 1 ? 'pending-pill' : ''}} {{$agency->approved == 0 ? 'inactive-pill' : ''}} text-center">
                                <h5 class="text-white" style="font-weight:800">{{$agency->approved == 2 ? 'Published' : ''}} {{$agency->approved == 1 ? 'Pending' : ''}} {{$agency->approved == 0 ? 'Inactive' : ''}}</h5>
                            </div>
                        </td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['AgenciesController@togglePublishAgency', $agency->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-primary px-1 py-1" style="font-weight:800">{{$agency->approved == 0 ? 'Publish' : 'Unpublish'}}</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editAgencyModal-{{$agency->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteAgencyModal-{{$agency->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showAgency == "approval")
<div class="card shadow">
    <div class="card-header">
        <h2>
        Agencies Awaiting Approval
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createAgencyModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="10%">User</th>
                    <th width="15%">Agency ID</th>
                    <th width="10%">Date Created</th>
                    <th width="50%">Title</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\Agency::where('approved', '=', '1')->get() as $agencyPending)
                    <tr>
                        <?php $agencyUser = App\User::where('id', '=', $agencyPending->user_id)->get()->pluck('name')?>
                        <td>{{ $agencyUser->first() }}</td>
                        <td>{{ $agencyPending->id }}</td>
                        <td>{{ Carbon\Carbon::parse($agencyPending->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $agencyPending->name }}</td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['AgenciesController@approveAgency', $agencyPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-success px-1 py-1" style="font-weight:800">Accept</button> 
                            {{ Form::close() }}
                            {{ Form::open(['action' => ['AgenciesController@rejectAgency', $agencyPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-danger px-1 py-1" style="font-weight:800">Reject</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editAgencyModal-{{$agencyPending->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteAgencyModal-{{$agencyPending->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showAgency == "edit-approval")
<div class="card shadow">
    <div class="card-header">
        <h2>Agency Edit Requests</h2>
    </div>
    <div class="card-body">
        @include('admin tabs.editRequestTab', ['editRequests' => Victorlap\Approvable\Approval::open()->ofClass(App\Agency::class)->get(), 'table' => 'agencies'])
    </div>
</div>
@else
<div class="card shadow mb-5">
    <div class="card-header">
        <h2>
        All Agencies
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createAgencyModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th width="5%">#</th>
                    <th width="15%">Name</th>
                    <th width="10%">Region</th>
                    <th width="10%">Province</th>
                    <th width="10%">Municipality</th>
                    <th width="10%">Congressional District</th>
                    <th width="10%">Phone Number</th>
                    <th width="10%">Fax Number</th>
                    <th width="10%">Email</th>
                    <th width="10%" class="text-right">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach(App\Agency::all()->where('approved', '=', '2') as $agency)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$agency->name}}</td>
                        <td>{{$agency->region ? $agency->region : '-'}}</td>
                        <td>{{$agency->province ? $agency->province : '-'}}</td>
                        <td>{{$agency->municipality ? $agency->municipality : '-'}}</td>
                        <td>{{$agency->district ? $agency->district : '-'}}</td>
                        <td>{{$agency->phone ? $agency->phone : '-'}}</td>
                        <td>{{$agency->fax ? $agency->fax : '-'}}</td>
                        <td>{{$agency->email ? $agency->email : '-'}}</td>
                        <td class="text-right">
                            @if(auth()->user()->user_level == 5)
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editAgencyModal-{{$agency->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteAgencyModal-{{$agency->id}}"><i class="fas fa-trash"></i></button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endif