@include('admin modals.sectorModals')

<div class="row mt-3">
    <a href="{{ route('pages.getAdmin', ['showSector' => 'all'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showSector=='all' || !request()->showSector ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            All Sectors <span class="badge badge-warning">{{App\Sector::all()->where('approved', '=', '2')->count()}}</span>
        </button>
    </a>
    <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
    <a href="{{ route('pages.getAdmin', ['showSector' => 'user'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showSector=='user' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            My Items <span class="badge badge-warning">{{App\Sector::all()->where('user_id','=',auth()->user()->id)->count()}}</span>
        </button>
    </a>
    @if(auth()->user()->user_level == 5)
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showSector' => 'approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showSector=='approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Awaiting Approval <span class="badge badge-{{$sectorApprovalCount > 0 ? 'danger' : 'warning'}}">{{$sectorApprovalCount}}</span>
            </button>
        </a>
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showSector' => 'edit-approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showSector=='edit-approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Sector Edit Requests <span class="badge badge-{{$sectorEditApprovalCount > 0 ? 'danger' : 'warning'}}">{{$sectorEditApprovalCount}}</span>
            </button>
        </a>
    @endif
</div>

@if(request()->showSector == 'user')
<div class="card shadow">
    <div class="card-header">
        <h2>My Sectors 
            <a type="button" class="btn btn-success text-right float-right" data-toggle="modal" data-target="#createSectorModal"><i class="fas fa-plus"></i> Add</a> 
        </h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="5%">Sector ID</th>
                    <th width="10%">Date Created</th>
                    <th width="60%">Name</th>
                    <th class="text-center" width="10%">Status</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\Sector::where('user_id', '=', Auth::user()->id)->get() as $sector)
                    <tr>
                        <td>{{ $sector->id }}</td>
                        <td>{{ Carbon\Carbon::parse($sector->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $sector->title }}</td>
                        <td>
                            <div class="d-none d-sm-block status-pill {{$sector->approved == 2 ? 'approved-pill' : ''}} {{$sector->approved == 1 ? 'pending-pill' : ''}} {{$sector->approved == 0 ? 'inactive-pill' : ''}} text-center">
                                <h5 class="text-white" style="font-weight:800">{{$sector->approved == 2 ? 'Published' : ''}} {{$sector->approved == 1 ? 'Pending' : ''}} {{$sector->approved == 0 ? 'Inactive' : ''}}</h5>
                            </div>
                        </td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['SectorsController@togglePublishSector', $sector->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-primary px-1 py-1" style="font-weight:800">{{$sector->approved == 0 ? 'Publish' : 'Unpublish'}}</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editSectorModal-{{$sector->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteSectorModal-{{$sector->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showSector == 'approval')
<div class="card shadow">
    <div class="card-header">
        <h2>
        Sectors Awaiting Approval
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createSectorModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="10%">User</th>
                    <th width="15%">Sector ID</th>
                    <th width="10%">Date Created</th>
                    <th width="50%">Title</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\Sector::where('approved', '=', '1')->get() as $sectorPending)
                    <tr>
                        <?php $sectorUser = App\User::where('id', '=', $sectorPending->user_id)->get()->pluck('name')?>
                        <td>{{ $sectorUser->first() }}</td>
                        <td>{{ $sectorPending->id }}</td>
                        <td>{{ Carbon\Carbon::parse($sectorPending->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $sectorPending->name }}</td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['SectorsController@approveSector', $sectorPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-success px-1 py-1" style="font-weight:800">Accept</button> 
                            {{ Form::close() }}
                            {{ Form::open(['action' => ['SectorsController@rejectSector', $sectorPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-danger px-1 py-1" style="font-weight:800">Reject</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editSectorModal-{{$sectorPending->id}}"><i class="fas fa-edit"></i></button>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteSectorModal-{{$sectorPending->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showSector == 'edit-approval')
<div class="card shadow">
    <div class="card-header">
        <h2>Sector Edit Requests</h2>
    </div>
    <div class="card-body">
        @include('admin tabs.editRequestTab', ['editRequests' => Victorlap\Approvable\Approval::open()->ofClass(App\Sector::class)->get(), 'table' => 'sectors'])
    </div>
</div>
@else
<div class="card shadow mb-5">
    <div class="card-header">
        <h2>
        All Sectors
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createSectorModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th width="10%">#</th>
                    <th width="40%">Sector</th>
                    <th width="40%">Industry</th>
                    <th width="10%" class="text-right">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach(App\Sector::all()->where('approved', '=', '2') as $sector)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$sector->name}}</td>
                        <td>{{$sector->industry->name}}</td>
                        <td class="text-right">
                            @if(auth()->user()->user_level == 5)
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editSectorModal-{{$sector->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteSectorModal-{{$sector->id}}"><i class="fas fa-trash"></i></button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endif