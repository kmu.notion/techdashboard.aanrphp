@include('admin modals.generatorModals')

<div class="row mt-3">
    <a href="{{ route('pages.getAdmin', ['showGenerator' => 'all'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showGenerator=='all' || !request()->showGenerator ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            All Generators <span class="badge badge-warning">{{App\Generator::all()->where('approved', '=', '2')->count()}}</span>
        </button>
    </a>
    <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
    <a href="{{ route('pages.getAdmin', ['showGenerator' => 'user'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showGenerator=='user' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            My Items <span class="badge badge-warning">{{App\Generator::all()->where('user_id','=',auth()->user()->id)->count()}}</span>
        </button>
    </a>
    @if(auth()->user()->user_level == 5)
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showGenerator' => 'approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showGenerator=='approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Awaiting Approval <span class="badge badge-{{$generatorsApprovalCount > 0 ? 'danger' : 'warning'}}">{{$generatorsApprovalCount}}</span>
            </button>
        </a>
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showGenerator' => 'edit-approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showGenerator=='edit-approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Generator Edit Requests <span class="badge badge-{{$generatorsEditApprovalCount > 0 ? 'danger' : 'warning'}}">{{$generatorsEditApprovalCount}}</span>
            </button>
        </a>
    @endif
</div>
@if(request()->showGenerator == "user")
<div class="card shadow">
    <div class="card-header">
        <h2>My Generators
            <a type="button" class="btn btn-success text-right float-right" data-toggle="modal" data-target="#createGeneratorModal"><i class="fas fa-plus"></i> Add</a> 
        </h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="5%">Generator ID</th>
                    <th width="10%">Date Created</th>
                    <th width="60%">Name</th>
                    <th class="text-center" width="10%">Status</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\Generator::where('user_id', '=', Auth::user()->id)->get() as $generator)
                    <tr>
                        <td>{{ $generator->id }}</td>
                        <td>{{ Carbon\Carbon::parse($generator->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $generator->name }}</td>
                        <td>
                            <div class="d-none d-sm-block status-pill {{$generator->approved == 2 ? 'approved-pill' : ''}} {{$generator->approved == 1 ? 'pending-pill' : ''}} {{$generator->approved == 0 ? 'inactive-pill' : ''}} text-center">
                                <h5 class="text-white" style="font-weight:800">{{$generator->approved == 2 ? 'Published' : ''}} {{$generator->approved == 1 ? 'Pending' : ''}} {{$generator->approved == 0 ? 'Inactive' : ''}}</h5>
                            </div>
                        </td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['GeneratorsController@togglePublishGenerator', $generator->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-primary px-1 py-1" style="font-weight:800">{{$generator->approved == 0 ? 'Publish' : 'Unpublish'}}</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editGeneratorModal-{{$generator->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteGeneratorModal-{{$generator->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showGenerator == "approval")
<div class="card shadow">
    <div class="card-header">
        <h2>
        Generators Awaiting Approval
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createGeneratorModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="10%">User</th>
                    <th width="15%">Generator ID</th>
                    <th width="10%">Date Created</th>
                    <th width="50%">Title</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\Generator::where('approved', '=', '1')->get() as $generatorPending)
                    <tr>
                        <?php $generatorUser = App\User::where('id', '=', $generatorPending->user_id)->get()->pluck('name')?>
                        <td>{{ $generatorUser->first() }}</td>
                        <td>{{ $generatorPending->id }}</td>
                        <td>{{ Carbon\Carbon::parse($generatorPending->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $generatorPending->name }}</td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['GeneratorsController@approveGenerator', $generatorPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-success px-1 py-1" style="font-weight:800">Accept</button> 
                            {{ Form::close() }}
                            {{ Form::open(['action' => ['GeneratorsController@rejectGenerator', $generatorPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-danger px-1 py-1" style="font-weight:800">Reject</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editGeneratorModal-{{$generatorPending->id}}"><i class="fas fa-edit"></i></button>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteGeneratorModal-{{$generatorPending->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showGenerator == "edit-approval")
<div class="card shadow">
    <div class="card-header">
        <h2>Generator Edit Requests</h2>
    </div>
    <div class="card-body">
        @include('admin tabs.editRequestTab', ['editRequests' => Victorlap\Approvable\Approval::open()->ofClass(App\Generator::class)->get(), 'table' => 'generators'])
    </div>
</div>
@else
<div class="card shadow mb-5">
    <div class="card-header">
        <h2>
        All Generators
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createGeneratorModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table" style="width:100%">
            <thead>
                <tr>
                    <th style="width:5%">#</th>
                    <th style="width:25%">Name</th>
                    <th style="width:10%">Latest Agency Affiliation</th>
                    <th style="width:20%">Address</th>
                    <th style="width:10%">Phone</th>
                    <th style="width:10%">Fax</th>
                    <th style="width:10%">Email</th>
                    <th style="width:10%" class="text-right">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach(App\Generator::all()->where('approved', '=', '2') as $generator)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$generator->name}}</td>
                        <td>{{$generator->agency->name ? $generator->agency->name : '-'}}</td>
                        <td>{{$generator->address ? $generator->address : '-'}}</td>
                        <td>{{$generator->phone ? $generator->phone : '-'}}</td>
                        <td>{{$generator->fax ? $generator->fax : '-'}}</td>
                        <td>{{$generator->email ? $generator->email : '-'}}</td>
                        <td class="text-right">
                            @if(auth()->user()->user_level == 5)
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editGeneratorModal-{{$generator->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteGeneratorModal-{{$generator->id}}"><i class="fas fa-trash"></i></button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endif