@include('admin modals.adopterModals')

<div class="row mt-3">
    <a href="{{ route('pages.getAdmin', ['showAdopter' => 'all'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showAdopter=='all' || !request()->showAdopter ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            All Adopters <span class="badge badge-warning">{{App\Adopter::all()->where('approved', '=', '2')->count()}}</span>
        </button>
    </a>
    <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
    <a href="{{ route('pages.getAdmin', ['showAdopter' => 'user'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showAdopter=='user' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            My Items <span class="badge badge-warning">{{App\Adopter::all()->where('user_id','=',auth()->user()->id)->count()}}</span>
        </button>
    </a>
    @if(auth()->user()->user_level == 5)
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showAdopter' => 'approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showAdopter=='approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Awaiting Approval <span class="badge badge-{{$adoptersApprovalCount > 0 ? 'danger' : 'warning'}}">{{$adoptersApprovalCount}}</span>
            </button>
        </a>
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showAdopter' => 'edit-approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showAdopter=='edit-approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Adopter Edit Requests <span class="badge badge-{{$adoptersEditApprovalCount > 0 ? 'danger' : 'warning'}}">{{$adoptersEditApprovalCount}}</span>
            </button>
        </a>
    @endif
</div>
@if(request()->showAdopter == "user")
<div class="card shadow">
    <div class="card-header">
        <h2>My Adopters
            <a type="button" class="btn btn-success text-right float-right" data-toggle="modal" data-target="#createAdopterModal"><i class="fas fa-plus"></i> Add</a> 
        </h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="5%">Adopter ID</th>
                    <th width="10%">Date Created</th>
                    <th width="60%">Name</th>
                    <th class="text-center" width="10%">Status</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\Adopter::where('user_id', '=', Auth::user()->id)->get() as $adopter)
                    <tr>
                        <td>{{ $adopter->id }}</td>
                        <td>{{ Carbon\Carbon::parse($adopter->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $adopter->name }}</td>
                        <td>
                            <div class="d-none d-sm-block status-pill {{$adopter->approved == 2 ? 'approved-pill' : ''}} {{$adopter->approved == 1 ? 'pending-pill' : ''}} {{$adopter->approved == 0 ? 'inactive-pill' : ''}} text-center">
                                <h5 class="text-white" style="font-weight:800">{{$adopter->approved == 2 ? 'Published' : ''}} {{$adopter->approved == 1 ? 'Pending' : ''}} {{$adopter->approved == 0 ? 'Inactive' : ''}}</h5>
                            </div>
                        </td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['AdoptersController@togglePublishAdopter', $adopter->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-primary px-1 py-1" style="font-weight:800">{{$adopter->approved == 0 ? 'Publish' : 'Unpublish'}}</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editAdopterModal-{{$adopter->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteAdopterModal-{{$adopter->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showAdopter == "approval")
<div class="card shadow">
    <div class="card-header">
        <h2>
        Adopters Awaiting Approval
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createAdopterModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="10%">User</th>
                    <th width="15%">Adopter ID</th>
                    <th width="10%">Date Created</th>
                    <th width="50%">Title</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\Adopter::where('approved', '=', '1')->get() as $adopterPending)
                    <tr>
                        <?php $adopterUser = App\User::where('id', '=', $adopterPending->user_id)->get()->pluck('name')?>
                        <td>{{ $adopterUser->first() }}</td>
                        <td>{{ $adopterPending->id }}</td>
                        <td>{{ Carbon\Carbon::parse($adopterPending->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $adopterPending->name }}</td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['AdoptersController@approveAdopter', $adopterPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-success px-1 py-1" style="font-weight:800">Accept</button> 
                            {{ Form::close() }}
                            {{ Form::open(['action' => ['AdoptersController@rejectAdopter', $adopterPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-danger px-1 py-1" style="font-weight:800">Reject</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editAdopterModal-{{$adopterPending->id}}"><i class="fas fa-edit"></i></button>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteAdopterModal-{{$adopterPending->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                @foreach(Victorlap\Approvable\Approval::ofClass(App\Adopter::class)->open()->get() as $adopterApprovals)
                    <tr>
                        <?php $adopterUser = App\User::where('id', '=', $adopterApprovals->user_id)->get()->pluck('name')?>
                        <td>{{ $adopterUser->first() }}</td>
                        <td>{{ $adopterApprovals->approvable_id }}</td>
                        <td>{{ Carbon\Carbon::parse($adopterApprovals->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $adopterApprovals->title }}</td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['AdoptersController@approveAdopter', $adopterApprovals->approvable_id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-success px-1 py-1" style="font-weight:800">Accept</button> 
                            {{ Form::close() }}
                            {{ Form::open(['action' => ['AdoptersController@rejectAdopter', $adopterApprovals->approvable_id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-danger px-1 py-1" style="font-weight:800">Reject</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editAdopterModal-{{$adopterApprovals->approvable_id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteAdopterModal-{{$adopterApprovals->approvable_id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showAdopter == "edit-approval")
<div class="card shadow">
    <div class="card-header">
        <h2>Adopter Edit Requests</h2>
    </div>
    <div class="card-body">
        @include('admin tabs.editRequestTab', ['editRequests' => Victorlap\Approvable\Approval::open()->ofClass(App\Adopter::class)->get(), 'table' => 'adopters'])
    </div>
</div>
@else
<div class="card shadow mb-5">
    <div class="card-header">
        <h2>
        All Adopters
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createAdopterModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th width="5%">#</th>
                    <th width="15%">Name</th>
                    <th width="10%">Adopter Type</th>
                    <th width="10%">Region</th>
                    <th width="10%">Province</th>
                    <th width="10%">City/Municipality</th>
                    <th width="10%">Phone Number</th>
                    <th width="10%">Fax Number</th>
                    <th width="10%">Email Address</th>
                    <th width="10%" class="text-right">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach(App\Adopter::all()->where('approved', '=', '2') as $adopter)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$adopter->name ? $adopter->name : '-'}}</td>
                        <td>{{$adopter->adopter_type->name ? $adopter->adopter_type->name : '-'}}</td>
                        <td>{{$adopter->region ? $adopter->region : '-'}}</td>
                        <td>{{$adopter->province ? $adopter->province : '-'}}</td>
                        <td>{{$adopter->municipality ? $adopter->municipality : '-'}}</td>
                        <td>{{$adopter->phone ? $adopter->phone : '-'}}</td>
                        <td>{{$adopter->fax ? $adopter->fax : '-'}}</td>
                        <td>{{$adopter->email ? $adopter->email : '-'}}</td>
                        <td class="text-right">
                            @if(auth()->user()->user_level == 5)
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editAdopterModal-{{$adopter->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteAdopterModal-{{$adopter->id}}"><i class="fas fa-trash"></i></button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endif