@include('admin modals.userMessageModals')

<div class="card shadow">
    <form action="{{ route('deleteMessages')}}" class="deleteForm" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="Delete">
    <div class="card-header">
        <i class="far fa-envelope" style="font-size:25px;"></i>
        <span style="font-size:22px;">User Messages 
            <span class="float-right">
                <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i> Delete Checked</button>
            </span>
        </span>
    </div>
    <div class="card-body">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Concern</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach(App\UserMessage::orderByDesc('created_at')->get() as $userMessage)
                    <tr>
                        <td style="text-align:center"><input class="form-check-input" type="checkbox" name="messages_check[]" value="{{$userMessage->id}}" id="flexCheckDefault"></td>
                        <td>{{ $userMessage->name }}</td>
                        <td>{{ $userMessage->email }}</td>
                        <td>
                            @if($userMessage->concern == 1)
                            <span class="badge badge-light" style="font-size:15px">Request</span>
                            @elseif($userMessage->concern == 2)
                            <span class="badge badge-info" style="font-size:15px">Bug Report</span>
                            @elseif($userMessage->concern == 3)
                            <span class="badge badge-warning" style="font-size:15px">Question</span>
                            @elseif($userMessage->concern == 4)
                            <span class="badge badge-secondary" style="font-size:15px">Others</span>
                            @endif
                        </td>
                        <td>
                            @if($userMessage->status == 0)
                            <span class="badge badge-danger" style="font-size:15px">Pending</span> 
                            <span id="badge-{{$userMessage->id}}" class="badge badge-warning" style="font-size:15px">Unread</span>
                            @elseif($userMessage->status == 1)
                            <span class="badge badge-danger" style="font-size:15px">Pending</span> 
                            <span class="badge badge-secondary" style="font-size:15px">Read</span>
                            @elseif($userMessage->status == 2)
                            <span class="badge badge-success" style="font-size:15px">Resolved</span>
                            @endif
                        </td>
                        <td>
                            <button class="btn btn-primary px-2 py-1" type="button" id="viewUserMessageButton" data-toggle="modal" data-target="#viewUserMessageModal-{{$userMessage->id}}"><i class="far fa-eye"></i> View Message</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </form>
</div>