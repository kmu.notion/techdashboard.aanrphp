<div class="table-responsive">
    <table class="table">
        <thead>
        <tr>
            <th width="10%">User</th>
            <th width="10%">Tech ID</th>
            <th width="15%">Date Created</th>
            <th width="15%">Field Edited</th>
            <th width="15%">Old Value</th>
            <th width="15%">New Value</th>
            <th class="text-center" width="20%">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($editRequests as $editRequest)
            <tr>
                <?php $technologyUser = App\User::where('id', '=', $editRequest->user_id)->get()->pluck('name')?>
                <td>{{ $technologyUser->first() }}</td>
                <td>{{ $editRequest->approvable_id }}</td>
                <td>{{ Carbon\Carbon::parse($editRequest->created_at)->format('M d,Y g:i:s A') }}</td>
                <td>{{ $editRequest->key }}</td>
                <td>{{ DB::table($table)->find($editRequest->approvable_id)->{$editRequest->key} }}</td>
                <td>{{ $editRequest->value }}</td>
                <td style="text-align:center">
                    {{ Form::open(['route' => ['approveApproval', $editRequest->id], 'method' => 'POST', 'style="display:inline"']) }}
                        <button class="btn btn-success px-1 py-1" style="font-weight:800">Accept</button> 
                    {{ Form::close() }}
                    {{ Form::open(['route' => ['rejectApproval', $editRequest->id], 'method' => 'POST', 'style="display:inline"']) }}
                        <button class="btn btn-danger px-1 py-1" style="font-weight:800">Reject</button> 
                    {{ Form::close() }}
                    {{-- <a href="{{route($route, ['id' => $editRequest->approvable_id])}}" class="btn btn-info px-2 py-1"><i class="fas fa-edit"></i></a> --}}
                    <button class="btn btn-danger px-2 py-1" data-toggle="modal" data-target="#editRequestModals-deleteEditRequest-{{$editRequest->id}}"><i class="fas fa-trash"></i></button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>