@include('admin modals.commodityModals')
<div class="row mt-3">
    <a href="{{ route('pages.getAdmin', ['showCommodity' => 'all'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showCommodity=='all' || !request()->showCommodity ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            All Commodities <span class="badge badge-warning">{{App\Commodity::all()->where('approved', '=', '2')->count()}}</span>
        </button>
    </a>
    <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
    <a href="{{ route('pages.getAdmin', ['showCommodity' => 'user'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showCommodity=='user' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            My Items <span class="badge badge-warning">{{App\Commodity::all()->where('user_id','=',auth()->user()->id)->count()}}</span>
        </button>
    </a>
    @if(auth()->user()->user_level == 5)
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showCommodity' => 'approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showCommodity=='approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Awaiting Approval <span class="badge badge-{{$commoditiesApprovalCount > 0 ? 'danger' : 'warning'}}">{{$commoditiesApprovalCount}}</span>
            </button>
        </a>
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showCommodity' => 'edit-approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showCommodity=='edit-approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Commodity Edit Requests <span class="badge badge-{{$commoditiesEditApprovalCount > 0 ? 'danger' : 'warning'}}">{{$commoditiesEditApprovalCount}}</span>
            </button>
        </a>
    @endif
</div>
@if(request()->showCommodity == 'user')
<div class="card shadow">
    <div class="card-header">
        <h2>My Commodities 
            <a type="button" class="btn btn-success text-right float-right" data-toggle="modal" data-target="#createCommodityModal"><i class="fas fa-plus"></i> Add</a> 
        </h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="5%">Commodity ID</th>
                    <th width="10%">Date Created</th>
                    <th width="60%">Name</th>
                    <th class="text-center" width="10%">Status</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\Commodity::where('user_id', '=', Auth::user()->id)->get() as $commodity)
                    <tr>
                        <td>{{ $commodity->id }}</td>
                        <td>{{ Carbon\Carbon::parse($commodity->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $commodity->name }}</td>
                        <td>
                            <div class="d-none d-sm-block status-pill {{$commodity->approved == 2 ? 'approved-pill' : ''}} {{$commodity->approved == 1 ? 'pending-pill' : ''}} {{$commodity->approved == 0 ? 'inactive-pill' : ''}} text-center">
                                <h5 class="text-white" style="font-weight:800">{{$commodity->approved == 2 ? 'Published' : ''}} {{$commodity->approved == 1 ? 'Pending' : ''}} {{$commodity->approved == 0 ? 'Inactive' : ''}}</h5>
                            </div>
                        </td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['CommoditiesController@togglePublishCommodity', $commodity->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-primary px-1 py-1" style="font-weight:800">{{$commodity->approved == 0 ? 'Publish' : 'Unpublish'}}</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editCommodityModal-{{$commodity->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteCommodityModal-{{$commodity->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showCommodity == 'approval')
<div class="card shadow">
    <div class="card-header">
        <h2>
        Commodities Awaiting Approval
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createCommodityModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="10%">User</th>
                    <th width="15%">Commodity ID</th>
                    <th width="10%">Date Created</th>
                    <th width="50%">Title</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\Commodity::where('approved', '=', '1')->get() as $commodityPending)
                    <tr>
                        <?php $commodityUser = App\User::where('id', '=', $commodityPending->user_id)->get()->pluck('name')?>
                        <td>{{ $commodityUser->first() }}</td>
                        <td>{{ $commodityPending->id }}</td>
                        <td>{{ Carbon\Carbon::parse($commodityPending->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $commodityPending->name }}</td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['CommoditiesController@approveCommodity', $commodityPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-success px-1 py-1" style="font-weight:800">Accept</button> 
                            {{ Form::close() }}
                            {{ Form::open(['action' => ['CommoditiesController@rejectCommodity', $commodityPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-danger px-1 py-1" style="font-weight:800">Reject</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editCommodityModal-{{$commodityPending->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteCommodityModal-{{$commodityPending->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showCommodity == 'edit-approval')
<div class="card shadow">
    <div class="card-header">
        <h2>Commodity Edit Requests</h2>
    </div>
    <div class="card-body">
        @include('admin tabs.editRequestTab', ['editRequests' => Victorlap\Approvable\Approval::open()->ofClass(App\Commodity::class)->get(), 'table' => 'commodities'])
    </div>
</div>
@else
<div class="card shadow mb-5">
    <div class="card-header">
        <h2>
        All Commodities
        <span class="float-right">
            <a type="button" class="btn btn-success text-right float-right" data-toggle="modal" data-target="#createCommodityModal"><i class="fas fa-plus"></i> Add</a>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th width="10%">#</th>
                    <th width="40%">Commodity</th>
                    <th width="40%">Industry - Sector</th>
                    <th width="10%" class="text-right">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach(App\Commodity::all()->where('approved', '=', '2') as $commodity)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$commodity->name}}</td>
                        <td>{{$commodity->sector->industry->name}} - {{$commodity->sector->name}}</td>
                        <td class="text-right">
                            @if(auth()->user()->user_level == 5)
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editCommodityModal-{{$commodity->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteCommodityModal-{{$commodity->id}}"><i class="fas fa-trash"></i></button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endif