@include('admin modals.technologyModals')

<div class="row mt-3">
    <a href="{{ route('pages.getAdmin', ['showTechnologies' => 'all'])}}" style="padding-left:15px">
        <button type="button" class="btn  {{ request()->showTechnologies=='all' || !request()->showTechnologies ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            All Technologies <span class="badge badge-warning">{{App\Technology::all()->where('approved', '=', '2')->count()}}</span>
        </button>
    </a>
    <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
    <a href="{{ route('pages.getAdmin', ['showTechnologies' => 'user'])}}" style="padding-left:15px">
        <button type="button" class="btn  {{ request()->showTechnologies=='user' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            My Items <span class="badge badge-warning">{{App\Technology::all()->where('user_id','=',auth()->user()->id)->count()}}</span>
        </button>
    </a>
    @if(auth()->user()->user_level == 5)
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showTechnologies' => 'approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showTechnologies=='approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Awaiting Approval <span class="badge badge-{{$techApprovalCount > 0 ? 'danger' : 'warning'}}">{{$techApprovalCount}}</span>
            </button>
        </a>
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showTechnologies' => 'edit-approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showTechnologies=='edit-approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Technology Edit Requests <span class="badge badge-{{$techEditApprovalCount > 0 ? 'danger' : 'warning'}}">{{$techEditApprovalCount}}</span>
            </button>
        </a>
    @endif
</div>
@if(request()->showTechnologies=='user')
<div class="card shadow">
    <div class="card-header">
        <h2>My Technologies 
            <a type="button" class="btn btn-success text-right float-right" href="/admin/tech/add"><i class="fas fa-plus"></i> Add</a> 
        </h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="5%">Tech ID</th>
                    <th width="10%">Date Created</th>
                    <th width="60%">Title</th>
                    <th class="text-center" width="10%">Status</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\Technology::where('user_id', '=', Auth::user()->id)->get() as $tech)
                    <tr>
                        <td>{{ $tech->id }}</td>
                        <td>{{ Carbon\Carbon::parse($tech->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $tech->title }}</td>
                        <td>
                            <div class="d-none d-sm-block status-pill {{$tech->approved == 2 ? 'approved-pill' : ''}} {{$tech->approved == 1 ? 'pending-pill' : ''}} {{$tech->approved == 0 ? 'inactive-pill' : ''}} text-center">
                                <h5 class="text-white" style="font-weight:800">{{$tech->approved == 2 ? 'Published' : ''}} {{$tech->approved == 1 ? 'Pending' : ''}} {{$tech->approved == 0 ? 'Inactive' : ''}}</h5>
                            </div>
                        </td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['TechnologiesController@togglePublishTechnology', $tech->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn {{$tech->approved == 0 ? 'btn-success' : 'btn-warning'}} px-1 py-1" style="font-weight:800">{{$tech->approved == 0 ? 'Publish' : 'Unpublish'}}</button> 
                            {{ Form::close() }}
                            <a href="/admin/tech/{{$tech->id}}/edit" class="btn btn-info px-2 py-1"><i class="fas fa-edit"></i></a>
                            <button class="btn btn-danger px-2 py-1" data-toggle="modal" data-target="#technologyModals-deleteTech-{{$tech->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showTechnologies=='approval')
<div class="card">
    <div class="card-header">
        <h2>Technologies Awaiting Approval 
        </h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="10%">User</th>
                    <th width="10%">Tech ID</th>
                    <th width="15%">Date Created</th>
                    <th width="45%">Title</th>
                    <th class="text-center" width="20%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\Technology::where('approved', '=', '1')->get() as $techNewPending)
                    <tr>
                        <?php $technologyUser = App\User::where('id', '=', $techNewPending->user_id)->get()->pluck('name')?>
                        <td>{{ $technologyUser->first() }}</td>
                        <td>{{ $techNewPending->id }}</td>
                        <td>{{ Carbon\Carbon::parse($techNewPending->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $techNewPending->title }}</td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['TechnologiesController@approveTechnology', $techNewPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-success px-1 py-1" style="font-weight:800">Accept</button> 
                            {{ Form::close() }}
                            {{ Form::open(['action' => ['TechnologiesController@rejectTechnology', $techNewPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-danger px-1 py-1" style="font-weight:800">Reject</button> 
                            {{ Form::close() }}
                            <a href="/admin/tech/{{$techNewPending->id}}/edit" class="btn btn-info px-2 py-1"><i class="fas fa-edit"></i></a>
                            <button class="btn btn-danger px-2 py-1" data-toggle="modal" data-target="#technologyModals-deleteTech-{{$techNewPending->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showTechnologies=='edit-approval')
<div class="card shadow">
    <div class="card-header">
        <h2>Technology Edit Requests</h2>
    </div>
    <div class="card-body">
        @include('admin tabs.editRequestTab', ['editRequests' => Victorlap\Approvable\Approval::open()->ofClass(App\Technology::class)->get(), 'route' => 'pages.techEditPage', 'table' => 'technologies'])
    </div>
</div>
@else
<div class="card shadow mb-5">
    <div class="card-header">
        <h2>All Technologies
        <span class="float-right">
            <a href="/admin/tech/add" class="btn btn-success"><i class="fas fa-plus"></i> Add</a>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <div class= "mb-2">
                Toggle column: 
                <a class="btn btn-primary toggle-vis" data-column="0">#</a> - 
                <a class="btn btn-primary toggle-vis" data-column="1">ID</a> - 
                <a class="btn btn-primary toggle-vis" data-column="2">Title</a> - 
                <a class="btn btn-primary toggle-vis" data-column="3">Technology Applicability - Region</a> - 
                <a class="btn btn-primary toggle-vis" data-column="4">Technology Applicability - Industry</a> - 
                <a class="btn btn-primary toggle-vis" data-column="5">Category</a> -
                <a class="btn btn-primary toggle-vis" data-column="6">Industry</a> -
                <a class="btn btn-primary toggle-vis" data-column="7">Sector</a> -
                <a class="btn btn-primary toggle-vis" data-column="8">Commodity</a> -
                <a class="btn btn-primary toggle-vis" data-column="9">Year</a> -
                <a class="btn btn-primary toggle-vis" data-column="10">TRL</a>
            </div>
            <table class="table tech-table" style="table-layout: fixed; width: 100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Technology Applicability - Region</th>
                        <th>Technology Applicability - Industry</th>
                        <th>Category</th>
                        <th>Industry</th>
                        <th>Sector</th>
                        <th>Commodity</th>
                        <th>Year</th>
                        <th>TRL</th>
                        <th style="display:none;">Description</th>
                        <th style="display:none;">Adopters</th>
                        <th style="display:none;">Generators</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(App\Technology::where('approved', '=', '2')->get() as $tech)
                        <tr data-toggle="modal" data-id="2" data-target="#technologyModals-viewTech-{{$tech->id}}">
                            <td>{{$loop->iteration}}</td>
                            <td>{{$tech->id}}</td>
                            <td>{{$tech->title}}</td>
                            <td>{{$tech->applicability_location}}</td>
                            <td>
                                @foreach($tech->applicability_industries as $applicability_industry)
                                    @if( $loop->first ) 
                                        {{ $applicability_industry->name }}
                                    @else
                                        <br> {{ $applicability_industry->name }}
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                @foreach($tech->technology_categories as $technology_category)
                                    @if( $loop->first ) 
                                        {{ $technology_category->name }}
                                    @else
                                        <br> {{ $technology_category->name }}
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                @foreach($tech->commodities as $commodity)
                                    @if( $loop->first ) 
                                        <i>{{ $commodity->sector->industry->name }} </i>
                                        @else
                                        <i> <br>{{ $commodity->sector->industry->name }} </i>
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                @foreach($tech->commodities as $commodity)
                                    @if( $loop->first ) 
                                        <i>{{ $commodity->sector->name }} </i>
                                    @else
                                        <i> <br>{{ $commodity->sector->name }} </i>
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                @foreach($tech->commodities as $commodity)
                                    @if( $loop->first ) 
                                        <i>{{ $commodity->name }} </i>
                                    @else
                                        <i><br> {{ $commodity->name }} </i>
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                {{$tech->year_developed}}
                            </td>
                            <td>
                                {{$tech->technology_readiness_id}}
                            </td>
                            <td style="display:none;">
                                {{$tech->description}}
                            </td>
                            <td style="display:none;">
                                @if(count($tech->adopters) != 0)
                                    @foreach($tech->adopters as $adopter)
                                        <span class="ml-3">• {{$adopter->name}} </span><br>
                                    @endforeach
                                @endif
                            </td>
                            <td style="display:none;">
                                @if(count($tech->generators) != 0)
                                    @foreach($tech->generators as $generator)
                                        <span class="ml-3">• {{$generator->name}} </span><br>
                                    @endforeach
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        <style>
             .toggle-vis{
                color:white !important;
                font-weight: bold;
            }
            .tech-table{
                overflow-y:scroll;
                height:500px;
                display:block;
            }
            td, th{
                word-wrap: break-word
            }
        </style>
        </div>
    </div>
</div>
@endif
<script>
    $(document).ready(function() {
        // init datatable on #example table
        var table = $('.tech-table').DataTable({
            columnDefs: [{
                targets: [11, 12, 13],
                searchable: true,
                visible: false
            }]
        });

        $('a.toggle-vis').on( 'click', function (e) {
            e.preventDefault();

            $(this).toggleClass("btn-primary btn-danger");
            // Get the column API object
            var column = table.column( $(this).attr('data-column') );
    
            // Toggle the visibility
            column.visible( ! column.visible() );
        });
    });
</script>