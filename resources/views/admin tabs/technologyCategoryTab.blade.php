@include('admin modals.technologyCategoryModals')

<div class="row mt-3">
    <a href="{{ route('pages.getAdmin', ['showTechCategory' => 'all'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showTechCategory=='all' || !request()->showTechCategory ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            All Technology Categories <span class="badge badge-warning">{{App\TechnologyCategory::all()->where('approved', '=', '2')->count()}}</span>
        </button>
    </a>
    <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
    <a href="{{ route('pages.getAdmin', ['showTechCategory' => 'user'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showTechCategory=='user' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            My Items <span class="badge badge-warning">{{App\TechnologyCategory::all()->where('user_id','=',auth()->user()->id)->count()}}</span>
        </button>
    </a>
    @if(auth()->user()->user_level == 5)
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showTechCategory' => 'approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showTechCategory=='approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Awaiting Approval <span class="badge badge-{{$techCategoriesApprovalCount > 0 ? 'danger' : 'warning'}}">{{$techCategoriesApprovalCount}}</span>
            </button>
        </a>
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showTechCategory' => 'edit-approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showTechCategory=='edit-approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Technology Category Edit Requests <span class="badge badge-{{$techCategoriesEditApprovalCount > 0 ? 'danger' : 'warning'}}">{{$techCategoriesEditApprovalCount}}</span>
            </button>
        </a>
    @endif
</div>
@if(request()->showTechCategory == "user")
<div class="card shadow">
    <div class="card-header">
        <h2>My Technology Categories 
            <a type="button" class="btn btn-success text-right float-right" data-toggle="modal" data-target="#createTechnologyCategoryModal"><i class="fas fa-plus"></i> Add</a> 
        </h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="5%">Technology Category ID</th>
                    <th width="10%">Date Created</th>
                    <th width="35%">Name</th>
                    <th class="text-center" width="25%">Status</th>
                    <th class="text-center" width="25%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\TechnologyCategory::where('user_id', '=', Auth::user()->id)->get() as $technologyCategory)
                    <tr>
                        <td>{{ $technologyCategory->id }}</td>
                        <td>{{ Carbon\Carbon::parse($technologyCategory->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $technologyCategory->name }}</td>
                        <td>
                            <div class="d-none d-sm-block status-pill {{$technologyCategory->approved == 2 ? 'approved-pill' : ''}} {{$technologyCategory->approved == 1 ? 'pending-pill' : ''}} {{$technologyCategory->approved == 0 ? 'inactive-pill' : ''}} text-center">
                                <h5 class="text-white" style="font-weight:800">{{$technologyCategory->approved == 2 ? 'Published' : ''}} {{$technologyCategory->approved == 1 ? 'Pending' : ''}} {{$technologyCategory->approved == 0 ? 'Inactive' : ''}}</h5>
                            </div>
                        </td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['TechnologyCategoriesController@togglePublishTechnologyCategory', $technologyCategory->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-primary px-1 py-1" style="font-weight:800">{{$technologyCategory->approved == 0 ? 'Publish' : 'Unpublish'}}</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editTechnologyCategoryModal-{{$technologyCategory->id}}"><i class="fas fa-edit" style="display:inline"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteTechnologyCategoryModal-{{$technologyCategory->id}}"><i class="fas fa-trash" style="display:inline"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showTechCategory == "approval")
<div class="card shadow">
    <div class="card-header">
        <h2>
        Technology Categories Awaiting Approval
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createTechnologyCategoryModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="10%">User</th>
                    <th width="15%">Technology Category ID</th>
                    <th width="10%">Date Created</th>
                    <th width="50%">Title</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\TechnologyCategory::where('approved', '=', '1')->get() as $technologyCategoryPending)
                    <tr>
                        <?php $technologyCategoryUser = App\User::where('id', '=', $technologyCategoryPending->user_id)->get()->pluck('name')?>
                        <td>{{ $technologyCategoryUser->first() }}</td>
                        <td>{{ $technologyCategoryPending->id }}</td>
                        <td>{{ Carbon\Carbon::parse($technologyCategoryPending->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $technologyCategoryPending->name }}</td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['TechnologyCategoriesController@approveTechnologyCategory', $technologyCategoryPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-success px-1 py-1" style="font-weight:800">Accept</button> 
                            {{ Form::close() }}
                            {{ Form::open(['action' => ['TechnologyCategoriesController@rejectTechnologyCategory', $technologyCategoryPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-danger px-1 py-1" style="font-weight:800">Reject</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editTechnologyCategoryModal-{{$technologyCategoryPending->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteTechnologyCategoryModal-{{$technologyCategoryPending->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showTechCategory == "edit-approval")
<div class="card shadow">
    <div class="card-header">
        <h2>Technology Category Edit Requests</h2>
    </div>
    <div class="card-body">
        @include('admin tabs.editRequestTab', ['editRequests' => Victorlap\Approvable\Approval::open()->ofClass(App\TechnologyCategory::class)->get(), 'table' => 'technology_categories'])
    </div>
</div>
@else
<div class="card shadow mb-5">
    <div class="card-header">
        <h2>
        All Technology Categories
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createTechnologyCategoryModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th width="10%">#</th>
                    <th width="80%">Name</th>
                    <th width="10%" class="text-right">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach(App\TechnologyCategory::all()->where('approved', '=', '2') as $technologyCategory)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$technologyCategory->name}}</td>
                        <td class="text-right">
                            @if(auth()->user()->user_level == 5)
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editTechnologyCategoryModal-{{$technologyCategory->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteTechnologyCategoryModal-{{$technologyCategory->id}}"><i class="fas fa-trash"></i></button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endif