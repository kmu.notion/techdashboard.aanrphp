@include('admin modals.applicabilityIndustryModals')

<div class="row mt-3">
    <a href="{{ route('pages.getAdmin', ['showApplicabilityIndustry' => 'all'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showApplicabilityIndustry=='all' || !request()->showApplicabilityIndustry ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            All Technology Applicability - Industries <span class="badge badge-warning">{{App\ApplicabilityIndustry::all()->where('approved', '=', '2')->count()}}</span>
        </button>
    </a>
    <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
    <a href="{{ route('pages.getAdmin', ['showApplicabilityIndustry' => 'user'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showApplicabilityIndustry=='user' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            My Items <span class="badge badge-warning">{{App\ApplicabilityIndustry::all()->where('user_id','=',auth()->user()->id)->count()}}</span>
        </button>
    </a>
    @if(auth()->user()->user_level == 5)
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showApplicabilityIndustry' => 'approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showApplicabilityIndustry=='approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Awaiting Approval <span class="badge badge-{{$applicabilityIndustryApprovalCount > 0 ? 'danger' : 'warning'}}">{{$applicabilityIndustryApprovalCount}}</span>
            </button>
        </a>
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showApplicabilityIndustry' => 'edit-approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showApplicabilityIndustry=='edit-approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Technology Applicability - Industries Edit Requests <span class="badge badge-{{$applicabilityIndustryEditApprovalCount > 0 ? 'danger' : 'warning'}}">{{$applicabilityIndustryEditApprovalCount}}</span>
            </button>
        </a>
    @endif
</div>
@if(request()->showApplicabilityIndustry == "user")
    <div class="card shadow">
        <div class="card-header">
            <h2>
            My Technology Applicability - Industries
            <span class="float-right">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createApplicabilityIndustryModal"><i class="fas fa-plus"></i> Add</button>
            </span></h2>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th width="10%">#</th>
                            <th width="70%">Name</th>
                            <th width="10%">Status</th>
                            <th width="10%" class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach(App\ApplicabilityIndustry::all()->where('user_id','=',auth()->user()->id)->reverse() as $applicabilityIndustry)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$applicabilityIndustry->name}}</td>
                                <td>
                                    <div class="d-none d-sm-block status-pill {{$applicabilityIndustry->approved == 2 ? 'approved-pill' : ''}} {{$applicabilityIndustry->approved == 1 ? 'pending-pill' : ''}} {{$applicabilityIndustry->approved == 0 ? 'inactive-pill' : ''}} text-center">
                                        <h5 class="text-white" style="font-weight:800">{{$applicabilityIndustry->approved == 2 ? 'Published' : ''}} {{$applicabilityIndustry->approved == 1 ? 'Pending' : ''}} {{$applicabilityIndustry->approved == 0 ? 'Inactive' : ''}}</h5>
                                    </div>
                                </td>
                                <td class="text-right">
                                    {{ Form::open(['action' => ['ApplicabilityIndustriesController@togglePublishApplicabilityIndustry', $applicabilityIndustry->id], 'method' => 'POST', 'style="display:inline"']) }}
                                        <button class="btn btn-primary px-1 py-1" style="font-weight:800">{{$applicabilityIndustry->approved == 0 ? 'Publish' : 'Unpublish'}}</button> 
                                    {{ Form::close() }}
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editApplicabilityIndustryModal-{{$applicabilityIndustry->id}}"><i class="fas fa-edit"></i></button>
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteApplicabilityIndustryModal-{{$applicabilityIndustry->id}}"><i class="fas fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@elseif(request()->showApplicabilityIndustry == "approval")
<div class="card shadow">
    <div class="card-header">
        <h2>
        Technology Applicability - Industries Awaiting Approval
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createApplicabilityIndustryModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="10%">User</th>
                    <th width="15%">Applicability - Industry ID</th>
                    <th width="10%">Date Created</th>
                    <th width="50%">Title</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse(App\ApplicabilityIndustry::where('approved', '=', '1')->get() as $applicabilityIndustryPending)
                    <tr>
                        <?php $industryUser = App\User::where('id', '=', $applicabilityIndustryPending->user_id)->get()->pluck('name')?>
                        <td>{{ $industryUser->first() }}</td>
                        <td>{{ $applicabilityIndustryPending->id }}</td>
                        <td>{{ Carbon\Carbon::parse($applicabilityIndustryPending->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $applicabilityIndustryPending->name }}</td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['ApplicabilityIndustriesController@approveApplicabilityIndustry', $applicabilityIndustryPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-success px-1 py-1" style="font-weight:800">Accept</button> 
                            {{ Form::close() }}
                            {{ Form::open(['action' => ['ApplicabilityIndustriesController@rejectApplicabilityIndustry', $applicabilityIndustryPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-danger px-1 py-1" style="font-weight:800">Reject</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editApplicabilityIndustryModal-{{$applicabilityIndustryPending->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteApplicabilityIndustryModal-{{$applicabilityIndustryPending->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showApplicabilityIndustry == "edit-approval")
<div class="card shadow">
    <div class="card-header">
        <h2>Technology Applicability - Industry Edit Requests</h2>
    </div>
    <div class="card-body">
        @include('admin tabs.editRequestTab', ['editRequests' => Victorlap\Approvable\Approval::open()->ofClass(App\ApplicabilityIndustry::class)->get(), 'table' => 'applicability_industries'])
    </div>
</div>
@else
<div class="card shadow mb-5">
    <div class="card-header">
        <h2>
        All Technology Applicability - Industry
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createApplicabilityIndustryModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th width="10%">#</th>
                        <th width="80%">Name</th>
                        <th width="10%" class="text-right">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(App\ApplicabilityIndustry::where('approved', '=', '2')->get()->reverse() as $applicabilityIndustry)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$applicabilityIndustry->name}}</td>
                            <td class="text-right">
                                @if(auth()->user()->user_level == 5)
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editApplicabilityIndustryModal-{{$applicabilityIndustry->id}}"><i class="fas fa-edit"></i></button>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteApplicabilityIndustryModal-{{$applicabilityIndustry->id}}"><i class="fas fa-trash"></i></button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif
