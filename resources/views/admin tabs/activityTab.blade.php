<div class="card shadow mb-5">
    <div class="card-header">
        <form action="{{ route('exportLog')}}" id="exportLog" method="GET">
        <h2>
        Activity Logs
            <span class="float-right">
                <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Download Excel</button>
            </span>
        </h2>
        </form>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table id="logs_table">
            <thead>
                <tr>
                    <th style="width:5%">#</th>
                    <th width="15%">Timestamp</th>
                    <th width="5%">User ID</th>
                    <th width="10%">User Name</th>
                    <th width="5%">User Level</th>
                    <th width="10%">IP Address</th>
                    <th width="15%">Resource</th>
                    <th width="35%">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach(App\Log::orderBy('id', 'asc')->get(); as $log)
                    <tr>
                        <td>{{$log->id}}</td>
                        <td>{{Carbon\Carbon::parse($log->created_at)->format('M d,Y g:i:s A')}}</td>
                        <td>{{$log->user_id}}</td>
                        <td>{{$log->user_name}}</td>
                        <td>{{$log->user_level}}</td>
                        <td>{{$log->IP_address}}</td>
                        <td>{{$log->resource}}</td>
                        <td>{{$log->action}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>