@include('admin modals.industryModals')

<div class="row mt-3">
    <a href="{{ route('pages.getAdmin', ['showIndustry' => 'all'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showIndustry=='all' || !request()->showIndustry ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            All Industries <span class="badge badge-warning">{{App\Industry::all()->where('approved', '=', '2')->count()}}</span>
        </button>
    </a>
    <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
    <a href="{{ route('pages.getAdmin', ['showIndustry' => 'user'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showIndustry=='user' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            My Items <span class="badge badge-warning">{{App\Industry::all()->where('user_id','=',auth()->user()->id)->count()}}</span>
        </button>
    </a>
    @if(auth()->user()->user_level == 5)
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showIndustry' => 'approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showIndustry=='approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Awaiting Approval <span class="badge badge-{{$industryApprovalCount > 0 ? 'danger' : 'warning'}}">{{$industryApprovalCount}}</span>
            </button>
        </a>
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showIndustry' => 'edit-approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showIndustry=='edit-approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Industry Edit Requests <span class="badge badge-{{$industryEditApprovalCount > 0 ? 'danger' : 'warning'}}">{{$industryEditApprovalCount}}</span>
            </button>
        </a>
    @endif
</div>
@if(request()->showIndustry=='user')
<div class="card shadow">
    <div class="card-header">
        <h2>My Industries 
            <a type="button" class="btn btn-success text-right float-right" data-toggle="modal" data-target="#createIndustryModal"><i class="fas fa-plus"></i> Add</a> 
        </h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="5%">Industry ID</th>
                    <th width="10%">Date Created</th>
                    <th width="60%">Name</th>
                    <th class="text-center" width="10%">Status</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\Industry::where('user_id', '=', Auth::user()->id)->get() as $industry)
                    <tr>
                        <td>{{ $industry->id }}</td>
                        <td>{{ Carbon\Carbon::parse($industry->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $industry->name }}</td>
                        <td>
                            <div class="d-none d-sm-block status-pill {{$industry->approved == 2 ? 'approved-pill' : ''}} {{$industry->approved == 1 ? 'pending-pill' : ''}} {{$industry->approved == 0 ? 'inactive-pill' : ''}} text-center">
                                <h5 class="text-white" style="font-weight:800">{{$industry->approved == 2 ? 'Published' : ''}} {{$industry->approved == 1 ? 'Pending' : ''}} {{$industry->approved == 0 ? 'Inactive' : ''}}</h5>
                            </div>
                        </td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['IndustriesController@togglePublishIndustry', $industry->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-primary px-1 py-1" style="font-weight:800">{{$industry->approved == 0 ? 'Publish' : 'Unpublish'}}</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editIndustryModal-{{$industry->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteIndustryModal-{{$industry->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showIndustry=='approval')
<div class="card shadow">
    <div class="card-header">
        <h2>
        Industries Awaiting Approval
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createIndustryModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="10%">User</th>
                    <th width="15%">Industry ID</th>
                    <th width="10%">Date Created</th>
                    <th width="50%">Title</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\Industry::where('approved', '=', '1')->get() as $industryPending)
                    <tr>
                        <?php $industryUser = App\User::where('id', '=', $industryPending->user_id)->get()->pluck('name')?>
                        <td>{{ $industryUser->first() }}</td>
                        <td>{{ $industryPending->id }}</td>
                        <td>{{ Carbon\Carbon::parse($industryPending->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $industryPending->name }}</td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['IndustriesController@approveIndustry', $industryPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-success px-1 py-1" style="font-weight:800">Accept</button> 
                            {{ Form::close() }}
                            {{ Form::open(['action' => ['IndustriesController@rejectIndustry', $industryPending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-danger px-1 py-1" style="font-weight:800">Reject</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editIndustryModal-{{$industryPending->id}}"><i class="fas fa-edit"></i></button>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteIndustryModal-{{$industryPending->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showIndustry == "edit-approval")
<div class="card shadow">
    <div class="card-header">
        <h2>Industry Edit Requests</h2>
    </div>
    <div class="card-body">
        @include('admin tabs.editRequestTab', ['editRequests' => Victorlap\Approvable\Approval::open()->ofClass(App\Industry::class)->get(), 'table' => 'industries'])
    </div>
</div>
@else
<div class="card shadow mb-5">
    <div class="card-header">
        <h2>
        All Industries
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createIndustryModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th width="10%">ID</th>
                        <th width="40%">Industry</th>
                        <th width="30%">Description</th>
                        <th width="10%">Background Color</th>
                        <th width="10%" class="text-right">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(App\Industry::all()->where('approved', '=', '2') as $industry)
                        <tr>
                            <td>{{$industry->id}}</td>
                            <td>{{$industry->name}}</td>
                            <td>{{$industry->description}}</td>
                            <td><input type="color" class="form-control form-control-color" value="{{$industry->backgroundcolor}}" disabled colorpallet="disabled"></td>
                            <td class="text-right">
                                @if(auth()->user()->user_level == 5)
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editIndustryModal-{{$industry->id}}"><i class="fas fa-edit"></i></button>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteIndustryModal-{{$industry->id}}"><i class="fas fa-trash"></i></button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif