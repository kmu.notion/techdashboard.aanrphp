<div class="card shadow mb-5">
    <div class="card-header">
        <h2>Access Levels</h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th width="10%">#</th>
                        <th width="40%">Name</th>
                        <th width="20%">Slug</th>
                        <th width="20%">Access Level</th>
                        <th width="10%" class="text-right">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(App\TechField::all() as $tech_field)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$tech_field->name}}</td>
                            <td>{{$tech_field->slug}}</td>
                            <td>{{$tech_field->level}}</td>
                            <td class="text-right">
                                @if(auth()->user()->user_level == 5)
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editTechFieldModal-{{$tech_field->id}}"><i class="fas fa-edit"></i></button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>