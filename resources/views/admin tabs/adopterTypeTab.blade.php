@include('admin modals.adopterTypeModals')

<div class="row mt-3">
    <a href="{{ route('pages.getAdmin', ['showAdopterType' => 'all'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showAdopterType=='all' || !request()->showAdopterType ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            All Adopter Types <span class="badge badge-warning">{{App\AdopterType::all()->where('approved', '=', '2')->count()}}</span>
        </button>
    </a>
    <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
    <a href="{{ route('pages.getAdmin', ['showAdopterType' => 'user'])}}" style="padding-left:15px">
        <button type="button" class="btn {{ request()->showAdopterType=='user' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
            My Items <span class="badge badge-warning">{{App\AdopterType::all()->where('user_id','=',auth()->user()->id)->count()}}</span>
        </button>
    </a>
    @if(auth()->user()->user_level == 5)
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showAdopterType' => 'approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showAdopterType=='approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Awaiting Approval <span class="badge badge-{{$adopterTypesApprovalCount > 0 ? 'danger' : 'warning'}}">{{$adopterTypesApprovalCount}}</span>
            </button>
        </a>
        <span style="font-size:1.5em">&nbsp;&nbsp;|</span>
        <a href="{{ route('pages.getAdmin', ['showAdopterType' => 'edit-approval'])}}" style="padding-left:15px">
            <button type="button" class="btn {{ request()->showAdopterType=='edit-approval' ? 'btn-primary' : 'btn-dark'}}" autocomplete="off">
                Adopter Type Edit Requests <span class="badge badge-{{$adopterTypesEditApprovalCount > 0 ? 'danger' : 'warning'}}">{{$adopterTypesEditApprovalCount}}</span>
            </button>
        </a>
    @endif
</div>
@if(request()->showAdopterType == "user")
<div class="card shadow">
    <div class="card-header">
        <h2>My Adopter Types 
            <a type="button" class="btn btn-success text-right float-right" data-toggle="modal" data-target="#createAdopterTypeModal"><i class="fas fa-plus"></i> Add</a> 
        </h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="5%">Adopter Type ID</th>
                    <th width="10%">Date Created</th>
                    <th width="60%">Name</th>
                    <th class="text-center" width="10%">Status</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\AdopterType::where('user_id', '=', Auth::user()->id)->get() as $adopterType)
                    <tr>
                        <td>{{ $adopterType->id }}</td>
                        <td>{{ Carbon\Carbon::parse($adopterType->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $adopterType->name }}</td>
                        <td>
                            <div class="d-none d-sm-block status-pill {{$adopterType->approved == 2 ? 'approved-pill' : ''}} {{$adopterType->approved == 1 ? 'pending-pill' : ''}} {{$adopterType->approved == 0 ? 'inactive-pill' : ''}} text-center">
                                <h5 class="text-white" style="font-weight:800">{{$adopterType->approved == 2 ? 'Published' : ''}} {{$adopterType->approved == 1 ? 'Pending' : ''}} {{$adopterType->approved == 0 ? 'Inactive' : ''}}</h5>
                            </div>
                        </td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['AdopterTypesController@togglePublishAdopterType', $adopterType->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-primary px-1 py-1" style="font-weight:800">{{$adopterType->approved == 0 ? 'Publish' : 'Unpublish'}}</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editAdopterTypeModal-{{$adopterType->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteAdopterTypeModal-{{$adopterType->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showAdopterType == "approval")
<div class="card shadow">
    <div class="card-header">
        <h2>
        Adopter Types Awaiting Approval
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createAdopterTypeModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="10%">User</th>
                    <th width="15%">Adopter Type ID</th>
                    <th width="10%">Date Created</th>
                    <th width="50%">Title</th>
                    <th class="text-center" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach(App\AdopterType::where('approved', '=', '1')->get() as $adopterTypePending)
                    <tr>
                        <?php $adopterTypeUser = App\User::where('id', '=', $adopterTypePending->user_id)->get()->pluck('name')?>
                        <td>{{ $adopterTypeUser->first() }}</td>
                        <td>{{ $adopterTypePending->id }}</td>
                        <td>{{ Carbon\Carbon::parse($adopterTypePending->created_at)->format('M d,Y g:i:s A') }}</td>
                        <td>{{ $adopterTypePending->name }}</td>
                        <td style="text-align:center">
                            {{ Form::open(['action' => ['AdopterTypesController@approveAdopterType', $adopterTypePending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-success px-1 py-1" style="font-weight:800">Accept</button> 
                            {{ Form::close() }}
                            {{ Form::open(['action' => ['AdopterTypesController@rejectAdopterType', $adopterTypePending->id], 'method' => 'POST', 'style="display:inline"']) }}
                                <button class="btn btn-danger px-1 py-1" style="font-weight:800">Reject</button> 
                            {{ Form::close() }}
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editAdopterTypeModal-{{$adopterTypePending->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteAdopterTypeModal-{{$adopterTypePending->id}}"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif(request()->showAdopterType == "edit-approval")
<div class="card shadow">
    <div class="card-header">
        <h2>Adopter Type Edit Requests</h2>
    </div>
    <div class="card-body">
        @include('admin tabs.editRequestTab', ['editRequests' => Victorlap\Approvable\Approval::open()->ofClass(App\AdopterType::class)->get(), 'table' => 'adopter_types'])
    </div>
</div>
@else
<div class="card shadow mb-5">
    <div class="card-header">
        <h2>
        All Adopter Types
        <span class="float-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createAdopterTypeModal"><i class="fas fa-plus"></i> Add</button>
        </span></h2>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th width="10%">#</th>
                    <th width="80%">Name</th>
                    <th width="10%" class="text-right">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach(App\AdopterType::all()->where('approved', '=', '2') as $adopterType)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$adopterType->name}}</td>
                        <td class="text-right">
                            @if(auth()->user()->user_level == 5)
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#editAdopterTypeModal-{{$adopterType->id}}"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteAdopterTypeModal-{{$adopterType->id}}"><i class="fas fa-trash"></i></button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endif