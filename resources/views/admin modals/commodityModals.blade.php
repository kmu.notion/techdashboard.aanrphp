<!-- Commodities -->
    <!-- modal for create commodity -->
    <div class="modal fade" id="createCommodityModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(['action' => 'CommoditiesController@addCommodity', 'method' => 'POST']) }}
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Create new Commodity</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {{Form::label('industry', 'Industry')}}
                        <select name="industry" class="form-control industry_select" id="industry" data-dependent="sector">
                            <option value=""> Select Industry </option>
                            @foreach(App\Industry::all() as $industry)
                                <option value="{{$industry->id}}">{{$industry->name}}</option>
                            @endforeach
                        </select> 
                    </div>
                    <div class="form-group">
                        {{Form::label('sector', 'Sector')}}
                        {{Form::select('sector', [], null, ['class' => 'form-control sector_select'])}}
                    </div>
                   
                    <div class="form-group">
                        {{Form::label('name', 'Commodity Name', ['class' => 'col-form-label'])}}
                        {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Add a name'])}}
                    </div>
                    {{ csrf_field() }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    {{Form::submit('Create Commodity', ['class' => 'btn btn-success'])}}
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
<!-- end of modal for create commodity -->

@foreach(App\Commodity::all() as $commodity)
    <!-- edit commodity -->
        <div class="modal fade" id="editCommodityModal-{{$commodity->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {{ Form::open(['action' => ['CommoditiesController@editCommodity', $commodity->id], 'method' => 'POST']) }}
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Edit Commodity</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            {{Form::label('industry', 'Industry')}}
                            <select name="industry" class="form-control industry_edit" id="industry" data-dependent="sector" data-identifier="{{$commodity->id}}">
                                <option value=""> Select Industry </option>
                                @foreach(App\Industry::all() as $industry)
                                    <option value="{{$industry->id}}" {{$industry->id == $commodity->sector->industry_id ? 'selected' : ''}}>{{$industry->name}}</option>
                                @endforeach
                            </select> 
                        </div>
                        <div class="form-group">
                            {{Form::label('sector', 'Sector')}}
                            {{Form::select('sector', $commodity->sector->industry->sectors->pluck('name', 'id'), $commodity->sector->id, ['class' => 'form-control sector_edit'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('name', 'Commodity Name', ['class' => 'col-form-label'])}}
                            {{Form::text('name', $commodity->name, ['class' => 'form-control', 'placeholder' => 'Add a name'])}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        {{Form::submit('Save Changes', ['class' => 'btn btn-success'])}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    <!-- edit commodity end -->

    <!-- confirm delete commodity -->
        <div class="modal fade" id="deleteCommodityModal-{{$commodity->id}}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="{{ route('deleteCommodity', $commodity->id) }}" class="deleteForm" method="POST">
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Confirm Delete</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <span>
                            <?php $comms = App\Commodity::with('technologies')->find($commodity->id); ?>
                            Are you sure you want to delete: <b>{{$commodity->name}}</b>?</br></br>
                            @if($comms->technologies->where('approved','=','2')->count() > 0)
                                The following technologies using this commodity will be affected:
                                <ul>
                                    @foreach($comms->technologies as $tech)
                                        <li>{{$tech->title}}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                        <input class="btn btn-danger" type="submit" value="Yes, Delete" onclick="this.disabled=true;this.form.submit();">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    <!-- confirm delete sector -->
@endforeach
<!-- Commodities END -->

<script>
    $('.industry_select').change( function() {
        if($(this).val != '') {
            $.ajax({
                url:"{{ route('industries.fetchSectors') }}",
                method:"GET",
                data:{
                    industry_id: $(this).val(),
                    _token: '{{csrf_token()}}',
                },
                success: function(result) {
                    $('.sector_select').html(result);
                }
            })
        }
    });

    $('.industry_edit').change( function() {
        if($(this).val != '') {
            $.ajax({
                url:"{{ route('industries.fetchSectors') }}",
                method:"GET",
                data:{
                    industry_id: $(this).val(),
                    _token: '{{csrf_token()}}',
                },
                success: function(result) {
                    $('.sector_edit').html(result);
                }
            })
        }
    });
</script>