<!-- User Messages -->
    @foreach(App\UserMessage::all() as $userMessage)
        <div class="modal fade messageModal" id="viewUserMessageModal-{{$userMessage->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 style="font-weight:900" class="modal-title" id="exampleModalLabel">User Message</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-5">
                                <b>Name</b><br>
                                {{$userMessage->name}}<br><br>
                                <b>Email</b><br>
                                {{$userMessage->email}}<br><br>
                                <b>Concern</b><br>
                                <select name="concern" class="form-control" disabled>
                                    <option value="1" {{$userMessage->concern == 1 ? 'selected' : ''}}>I am requesting for data</option>
                                    <option value="2" {{$userMessage->concern == 2 ? 'selected' : ''}}>I want to report a bug</option>
                                    <option value="3" {{$userMessage->concern == 3 ? 'selected' : ''}}>I have a question</option>
                                    <option value="4" {{$userMessage->concern == 4 ? 'selected' : ''}}>Others</option>
                                </select><br><br>
                            </div>
                            <div class="col-sm-7">
                                <b>Message</b><br>
                                {{$userMessage->message}}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        @if($userMessage->status!=2)
                        {{ Form::open(['action' => ['UserMessagesController@resolveMessage', $userMessage->id], 'method' => 'POST']) }}
                        {{Form::submit('Resolve', ['class' => 'btn btn-success'])}}
                        {{ Form::close() }}
                        @endif
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
<!-- User Messages end -->
