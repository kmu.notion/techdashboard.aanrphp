<!-- Agency -->
    <!-- create agency -->
    <div class="modal fade" id="createAgencyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(['action' => 'AgenciesController@addAgency', 'method' => 'POST']) }}
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Create new Agency</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {{Form::label('name', 'Technology Owner', ['class' => 'col-form-label'])}}
                        {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Add a name'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('region', 'Region')}}
                        {{Form::select('region', ['ARMM' => 'ARMM - Autonomous Region in Muslim Mindanao', 
                                                                    'CAR' => 'CAR - Cordillera Adminisitrative Region', 
                                                                    'NCR' => 'NCR - National Capital Region', 
                                                                    'Region 1' => 'Region 1 - Ilocos Region', 
                                                                    'Region 2' => 'Region 2 - Cagayan Valley', 
                                                                    'Region 3' => 'Region 3 - Central Luzon', 
                                                                    'Region 4A' => 'Region 4A - CALABARZON', 
                                                                    'Region 4B' => 'Region 4B - MIMAROPA', 
                                                                    'Region 5' => 'Region 5 - Bicol Region', 
                                                                    'Region 6' => 'Region 6 - Western Visayas', 
                                                                    'Region 7' => 'Region 7 - Central Visayas', 
                                                                    'Region 8' => 'Region 8 - Eastern Visayas', 
                                                                    'Region 9' => 'Region 9 - Zamboanga Peninsula', 
                                                                    'Region 10' => 'Region 10 - Northern Mindanao', 
                                                                    'Region 11' => 'Region 11 - Davao Region', 
                                                                    'Region 12' => 'Region 12 - SOCCKSARGEN', 
                                                                    'Region 13' => 'Region 13 - Caraga Region'
                                                                    ], '',['class' => 'form-control', 'placeholder' => '------------']) }}
                    </div>
                    <div class="form-group">
                        {{Form::label('province', 'Province', ['class' => 'col-form-label'])}}
                        {{Form::text('province', '', ['class' => 'form-control', 'placeholder' => 'Add province'])}}
                    </div>

                    <div class="form-group">
                        {{Form::label('municipality', 'Municipality', ['class' => 'col-form-label'])}}
                        {{Form::text('municipality', '', ['class' => 'form-control', 'placeholder' => 'Add municipality'])}}
                    </div>

                    <div class="form-group">
                        {{Form::label('district', 'District', ['class' => 'col-form-label'])}}
                        {{Form::text('district', '', ['class' => 'form-control', 'placeholder' => 'Add district'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('phone', 'Phone Number', ['class' => 'col-form-label'])}}
                        {{Form::text('phone', '', ['class' => 'form-control', 'placeholder' => 'Add phone number'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('fax', 'Fax Number', ['class' => 'col-form-label'])}}
                        {{Form::text('fax', '', ['class' => 'form-control', 'placeholder' => 'Add fax number'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('email', 'Email', ['class' => 'col-form-label'])}}
                        {{Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'Add email'])}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    {{Form::submit('Add Agency', ['class' => 'btn btn-success'])}}
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
<!-- create agency end -->

@foreach(App\Agency::all() as $agency)
    <!-- edit agency -->
        <div class="modal fade" id="editAgencyModal-{{$agency->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {{ Form::open(['action' => ['AgenciesController@editAgency', $agency->id], 'method' => 'POST']) }}
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Edit Agency</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            {{Form::label('name', 'Technology Owner', ['class' => 'col-form-label'])}}
                            {{Form::text('name', $agency->name, ['class' => 'form-control', 'placeholder' => 'Add a name'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('region', 'Region')}}
                            {{Form::select('region', ['ARMM' => 'ARMM - Autonomous Region in Muslim Mindanao', 
                                                                        'CAR' => 'CAR - Cordillera Adminisitrative Region', 
                                                                        'NCR' => 'NCR - National Capital Region', 
                                                                        'Region 1' => 'Region 1 - Ilocos Region', 
                                                                        'Region 2' => 'Region 2 - Cagayan Valley', 
                                                                        'Region 3' => 'Region 3 - Central Luzon', 
                                                                        'Region 4A' => 'Region 4A - CALABARZON', 
                                                                        'Region 4B' => 'Region 4B - MIMAROPA', 
                                                                        'Region 5' => 'Region 5 - Bicol Region', 
                                                                        'Region 6' => 'Region 6 - Western Visayas', 
                                                                        'Region 7' => 'Region 7 - Central Visayas', 
                                                                        'Region 8' => 'Region 8 - Eastern Visayas', 
                                                                        'Region 9' => 'Region 9 - Zamboanga Peninsula', 
                                                                        'Region 10' => 'Region 10 - Northern Mindanao', 
                                                                        'Region 11' => 'Region 11 - Davao Region', 
                                                                        'Region 12' => 'Region 12 - SOCCKSARGEN', 
                                                                        'Region 13' => 'Region 13 - Caraga Region'
                                                                        ], $agency->region,['class' => 'form-control', 'placeholder' => '------------']) }}
                        </div>
                        <div class="form-group">
                            {{Form::label('province', 'Province', ['class' => 'col-form-label'])}}
                            {{Form::text('province', $agency->province, ['class' => 'form-control', 'placeholder' => 'Add province'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('municipality', 'Municipality', ['class' => 'col-form-label'])}}
                            {{Form::text('municipality', $agency->municipality, ['class' => 'form-control', 'placeholder' => 'Add municipality'])}}
                        </div>

                        <div class="form-group">
                            {{Form::label('district', 'District', ['class' => 'col-form-label'])}}
                            {{Form::text('district', $agency->district, ['class' => 'form-control', 'placeholder' => 'Add district'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('phone', 'Phone Number', ['class' => 'col-form-label'])}}
                            {{Form::text('phone', $agency->phone, ['class' => 'form-control', 'placeholder' => 'Add phone number'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('fax', 'Fax Number', ['class' => 'col-form-label'])}}
                            {{Form::text('fax', $agency->fax, ['class' => 'form-control', 'placeholder' => 'Add fax number'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('email', 'Email', ['class' => 'col-form-label'])}}
                            {{Form::text('email', $agency->email, ['class' => 'form-control', 'placeholder' => 'Add email'])}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        {{Form::submit('Save Changes', ['class' => 'btn btn-success'])}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    <!-- edit agency end -->

    <!-- confirm delete agency -->
        <div class="modal fade" id="deleteAgencyModal-{{$agency->id}}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="{{ route('deleteAgency', $agency->id) }}" class="deleteForm" method="POST">
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Confirm Delete</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <span>
                            <?php $agens = App\Agency::with('technologies')->find($agency->id); ?>
                            Are you sure you want to delete: <b>{{$agency->name}}</b>?</br></br>
                            @if($agens->technologies->where('approved','=','2')->count() > 0)
                                The following technologies using this agency will be affected:
                                <ul>
                                    @foreach($agens->technologies as $tech)
                                        <li>{{$tech->title}}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                        <input class="btn btn-danger" type="submit" value="Yes, Delete" onclick="this.disabled=true;this.form.submit();">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    <!-- confirm delete agency -->
@endforeach
<!-- Agency End -->
