<!-- Adopter Types -->
    <!-- create adopter type -->
        <div class="modal fade" id="createAdopterTypeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {{ Form::open(['action' => 'AdopterTypesController@addAdopterType', 'method' => 'POST']) }}
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Create new Adopter Type</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            {{Form::label('name', 'Name', ['class' => 'col-form-label'])}}
                            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Add a name'])}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        {{Form::submit('Add Adopter Type', ['class' => 'btn btn-success'])}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    <!-- create adopter type end -->
    @foreach(App\AdopterType::all() as $adopterType)
        <!-- edit adopter type -->
            <div class="modal fade" id="editAdopterTypeModal-{{$adopterType->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        {{ Form::open(['action' => ['AdopterTypesController@editAdopterType', $adopterType->id], 'method' => 'POST']) }}
                        <div class="modal-header">
                            <h6 class="modal-title" id="exampleModalLabel">Edit Adopter Type</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                {{Form::label('name', 'Name', ['class' => 'col-form-label'])}}
                                {{Form::text('name', $adopterType->name, ['class' => 'form-control', 'placeholder' => 'Add a name'])}}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            {{Form::submit('Save Changes', ['class' => 'btn btn-success'])}}
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        <!-- edit adopter type end -->

        <!-- confirm delete adopter type -->
            <div class="modal fade" id="deleteAdopterTypeModal-{{$adopterType->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form action="{{ route('deleteAdopterType', $adopterType->id) }}" class="deleteForm" method="POST">
                        <div class="modal-header">
                            <h6 class="modal-title" id="exampleModalLabel">Confirm Delete</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <span>
                                <?php $adopterTypes = App\AdopterType::with('adopters')->find($adopterType->id); ?>
                                @if($adopterTypes->adopters->where('approved', '=', '2')->count() > 0)
                                    You cannot delete: <b>{{$adopterType->name}}</b></br></br>
                                    The following adopters needs to be deleted before deleting this adopter type:
                                    <ul>
                                        @foreach($adopterTypes->adopters as $adopts)
                                            <li>{{$adopts->name}}</li>
                                        @endforeach
                                    </ul>
                                @else
                                    Are you sure you want to delete: <b>{{$adopterType->name}}</b>?</br></br>
                                @endif
                            </span>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                            @if($adopterTypes->adopters->where('approved', '=', '2')->count() == 0)
                            <input class="btn btn-danger" type="submit" value="Yes, Delete" onclick="this.disabled=true;this.form.submit();">
                            @endif
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        <!-- confirm delete adopter type -->
    @endforeach
<!-- Adopter Types End -->
