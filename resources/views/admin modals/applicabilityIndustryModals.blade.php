<!-- Technology Applicability - Industries -->
    <!-- create applicability industry -->
    <div class="modal fade" id="createApplicabilityIndustryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(['action' => 'ApplicabilityIndustriesController@addApplicabilityIndustry', 'method' => 'POST']) }}
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Create new Technology Applicability - Industry</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {{Form::label('name', 'Name', ['class' => 'col-form-label'])}}
                        {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Add a name'])}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    {{Form::submit('Add Technology Applicability - Industry', ['class' => 'btn btn-success'])}}
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
<!-- create applicability industry end -->
@foreach(App\ApplicabilityIndustry::all() as $applicabilityIndustry)
    <!-- edit applicability applicability industry -->
        <div class="modal fade" id="editApplicabilityIndustryModal-{{$applicabilityIndustry->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {{ Form::open(['action' => ['ApplicabilityIndustriesController@editApplicabilityIndustry', $applicabilityIndustry->id], 'method' => 'POST']) }}
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Edit Industry</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            {{Form::label('name', 'Name', ['class' => 'col-form-label'])}}
                            {{Form::text('name', $applicabilityIndustry->name, ['class' => 'form-control', 'placeholder' => 'Add a name'])}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        {{Form::submit('Save Changes', ['class' => 'btn btn-success'])}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    <!-- edit applicability industry end -->

    <!-- confirm delete applicability industry -->
        <div class="modal fade" id="deleteApplicabilityIndustryModal-{{$applicabilityIndustry->id}}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="{{ route('deleteApplicabilityIndustry', $applicabilityIndustry->id) }}" class="deleteForm" method="POST">
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Confirm Delete</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <span>
                            <?php $techApp = DB::table('applicability_industry_technology')->where('applicability_industry_id', '=',$applicabilityIndustry->id)->get(); ?>
                            <?php $tech = App\Technology::all()?>
                            @if($techApp->count() > 0)
                                You cannot delete: <b>{{$applicabilityIndustry->name}}</b></br></br>
                                The following technologies are still using this Technology Applicability - Industry:
                                <ul>
                                    @foreach($techApp as $techID)
                                        <li>{{$tech->find($techID->technology_id)->title}}</li>
                                    @endforeach
                                </ul>
                            @else
                                Are you sure you want to delete: <b>{{$applicabilityIndustry->name}}</b>?</br></br>
                            @endif
                        </span>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                        @if($techApp->count() == 0)
                        <input class="btn btn-danger" type="submit" value="Yes, Delete" onclick="this.disabled=true;this.form.submit();">
                        @endif
                    </div>
                    </form>
                </div>
            </div>
        </div>
    <!-- confirm applicability delete industry -->
@endforeach
<!-- Technology Applicability - Industries END -->
