<!-- Technologies -->
    <!-- TECH VIEW MODAL -->
    @foreach(App\Technology::all() as $tech)
    <div class="modal fade" id="technologyModals-viewTech-{{$tech->id}}" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content pl-0 pr-0 pl-0">
                <div id="popup-details-{{$tech->id}}" class="inner-modal pl-3 pr-3"> 
                    <div class="modal-header">
                        <span style="width:100%" class="mt-2">
                            <h4>{{$tech->title}} </h4>
                        <span>
                    </div>
                    <div class="modal-body">
                        <small class="text-muted">
                            @if($tech->applicability_location != null)
                            <i class="fas fa-map-marker-alt"></i> {{$tech->applicability_location}}
                            @endif
                            @if($tech->year_developed != null)
                                <i class="far fa-calendar-alt ml-2"></i> {{$tech->year_developed}}
                            @endif
                        </small>
                        <br>
                        <div class="dropdown-divider mt-3"></div>
                        <b>Description</b><br>
                        <span>{{$tech->description}}</span>
                        @if($tech->significance != NULL)
                            <div class="dropdown-divider mt-3"></div>
                            <b>Significance</b><br>
                            <span>{{$tech->significance}}</span>
                        @endif
                        @if($tech->target_users != NULL)
                            <div class="dropdown-divider mt-3"></div>
                            <b>Target Users</b><br>
                            <span>{{$tech->target_users}}</span>
                        @endif
                        <div class="dropdown-divider mt-3"></div>
                        <b>Technology Applicability - Industry</b>
                        <span class="ml-3">
                            @foreach($tech->applicability_industries as $applicability_industry)
                                <br> • {{$applicability_industry->name}}
                            @endforeach
                        </span>
                        <div class="dropdown-divider mt-3"></div>
                        <b>Industry - Sector - Commodity</b><br>
                        @foreach($tech->commodities as $commodity)
                            <span class="ml-3">•
                                {{ $commodity->sector->industry->name }}
                                <i class="fas fa-angle-double-right"></i>
                                {{ $commodity->sector->name }}
                                <i class="fas fa-angle-double-right"></i>
                                {{ $commodity->name }}
                            </span><br>
                        @endforeach
                        @if(count($tech->agencies) != 0)
                        <div class="dropdown-divider mt-3"></div>
                        <b>Technology Owner</b><br>
                        @foreach($tech->agencies as $agency)
                            <span class="ml-3">• {{$agency->name}} </span><br>
                        @endforeach
                        @endif
                        @if(count($tech->generators) != 0)
                        <div class="dropdown-divider mt-3"></div>
                        <b>Technology Generator</b><br>
                        @foreach($tech->generators as $generator)
                            <span class="ml-3">• {{$generator->name}} </span><br>
                        @endforeach
                        @endif
                        @if(count($tech->adopters) != 0)
                        <div class="dropdown-divider mt-3"></div>
                        <b>Technology Adopters</b><br>
                        @foreach($tech->adopters as $adopter)
                            <span class="ml-3">• {{$adopter->name}} </span><br>
                        @endforeach
                        @endif
                        @if($tech->basic_research_title != null)
                        <div class="dropdown-divider mt-3"></div>
                        <b>Basic Research</b><br>
                        Project Title - {{$tech->basic_research_title}}<br>
                        Funding Agency - {{$tech->basic_research_funding}}<br>
                        Implementing Agency - {{$tech->basic_research_implementing}}<br>
                        Project Cost - {{$tech->basic_research_cost}}<br>
                        Start date - {{$tech->basic_research_start_date}}<br>
                        End date - {{$tech->basic_research_end_date}}
                        <div class="dropdown-divider mt-3"></div>
                        @endif
                        @if($tech->applied_research_title != null)
                        <b>Applied Research</b><br>
                        Project Title - {{$tech->applied_research_title}}<br>
                        Funding Agency - {{$tech->applied_research_funding}}<br>
                        Implementing Agency - {{$tech->applied_research_implementing}}<br>
                        Project Cost - {{$tech->applied_research_cost}}<br>
                        Start date - {{$tech->basic_research_start_date}}<br>
                        End date - {{$tech->basic_research_end_date}}
                        @endif
                    </div>
                    <div class="modal-footer">
                        @if(auth()->user()->user_level == 5)
                        <a href="/admin/tech/{{$tech->id}}/edit" class="btn btn-secondary">Edit</a>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#technologyModals-deleteTech-{{$tech->id}}-{{$tech->id}}">Delete</button>
                        @endif
                        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- confirm delete technology -->
        <div class="modal fade" id="technologyModals-deleteTech-{{$tech->id}}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="{{ route('deleteTechnology', $tech->id) }}" class="deleteForm" method="POST">
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Confirm Delete</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <span>
                            Are you sure you want to delete: <b>{{$tech->title}}</b>
                        </span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                        <input class="btn btn-danger" type="submit" value="Yes, Delete" onclick="this.disabled=true;this.form.submit();">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    <!-- confirm delete technology -->
@endforeach
<!-- END OF TECH VIEW MODAL -->
<!-- Technologies END -->
