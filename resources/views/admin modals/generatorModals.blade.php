<!-- Generator -->
    <!-- create generator -->
    <div class="modal fade" id="createGeneratorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(['action' => 'GeneratorsController@addGenerator', 'method' => 'POST']) }}
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Create new Generator</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {{Form::label('name', 'Name', ['class' => 'col-form-label'])}}
                        {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Add generator name'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('agency', 'Latest Agency Affiliation', ['class' => 'col-form-label'])}} 
                        {{Form::select('agency', $agencies, null, ['class' => 'form-control', 'placeholder' => 'Add agency'])}}        
                    </div>
                    <div class="form-group">
                        {{Form::label('address', 'Address', ['class' => 'col-form-label'])}}
                        {{Form::text('address', '', ['class' => 'form-control', 'placeholder' => 'Add address'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('phone', 'Phone Number', ['class' => 'col-form-label'])}}
                        {{Form::text('phone', '', ['class' => 'form-control', 'placeholder' => 'Add phone number'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('fax', 'Fax Number', ['class' => 'col-form-label'])}}
                        {{Form::text('fax', '', ['class' => 'form-control', 'placeholder' => 'Add fax number'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('email', 'Email Address', ['class' => 'col-form-label'])}}
                        {{Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'Add email address'])}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    {{Form::submit('Add Generator', ['class' => 'btn btn-success'])}}
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
<!-- create generator end -->

@foreach(App\Generator::all() as $generator)
    <!-- edit generator -->
        <div class="modal fade" id="editGeneratorModal-{{$generator->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {{ Form::open(['action' => ['GeneratorsController@editGenerator', $generator->id], 'method' => 'POST']) }}
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Edit Generator</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group ">
                            {{Form::label('name', 'Name', ['class' => 'col-form-label'])}}
                            {{Form::text('name', $generator->name, ['class' => 'form-control', 'placeholder' => 'Add generator name'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('agency', 'Latest Agency Affiliation', ['class' => 'col-form-label'])}} 
                            {{Form::select('agency', $agencies, $generator->agency, ['class' => 'form-control'])}}        
                        </div>
                        <div class="form-group">
                            {{Form::label('address', 'Address', ['class' => 'col-form-label'])}}
                            {{Form::text('address', $generator->address, ['class' => 'form-control', 'placeholder' => 'Add address'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('phone', 'Phone Number', ['class' => 'col-form-label'])}}
                            {{Form::text('phone', $generator->phone, ['class' => 'form-control', 'placeholder' => 'Add phone number'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('fax', 'Fax Number', ['class' => 'col-form-label'])}}
                            {{Form::text('fax', $generator->fax, ['class' => 'form-control', 'placeholder' => 'Add fax number'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('email', 'Email Address', ['class' => 'col-form-label'])}}
                            {{Form::text('email', $generator->email, ['class' => 'form-control', 'placeholder' => 'Add email address'])}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        {{Form::submit('Save Changes', ['class' => 'btn btn-success'])}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    <!-- edit generator end -->

    <!-- confirm delete generator -->
        <div class="modal fade" id="deleteGeneratorModal-{{$generator->id}}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="{{ route('deleteGenerator', $generator->id) }}" class="deleteForm" method="POST">
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Confirm Delete</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <span>
                            <?php $gens = App\Generator::with('technologies')->find($generator->id); ?>
                            Are you sure you want to delete: <b>{{$generator->name}}</b>?</br></br>
                            @if($gens->technologies->where('approved','=','2')->count() > 0)
                                The following technologies using this generator will be affected:
                                <ul>
                                    @foreach($gens->technologies as $tech)
                                        <li>{{$tech->title}}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                        <input class="btn btn-danger" type="submit" value="Yes, Delete" onclick="this.disabled=true;this.form.submit();">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    <!-- confirm delete generator -->
@endforeach
<!-- Generator End -->
