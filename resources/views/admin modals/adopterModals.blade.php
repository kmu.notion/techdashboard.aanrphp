<!-- Adopter -->
    <!-- create adopter -->
    <div class="modal fade" id="createAdopterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(['action' => 'AdoptersController@addAdopter', 'method' => 'POST']) }}
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Create new Adopter</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {{Form::label('name', 'Name', ['class' => 'col-form-label'])}}
                        {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Add a name'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('adopter_type', 'Adopter Type')}}
                        <select name="adopter_type" class="form-control" id="adopter_type">
                            <option value=""> Select Adopter Type </option>
                            @foreach(App\AdopterType::all() as $adopterType)
                                <option value="{{$adopterType->id}}">{{$adopterType->name}}</option>
                            @endforeach
                        </select> 
                    </div>
                    <div class="form-group">
                        {{Form::label('region', 'Region', ['class' => 'col-form-label'])}}
                        {{Form::select('region', ['ARMM' => 'ARMM - Autonomous Region in Muslim Mindanao', 
                                                    'CAR' => 'CAR - Cordillera Adminisitrative Region', 
                                                    'NCR' => 'NCR - National Capital Region', 
                                                    'Region 1' => 'Region 1 - Ilocos Region', 
                                                    'Region 2' => 'Region 2 - Cagayan Valley', 
                                                    'Region 3' => 'Region 3 - Central Luzon', 
                                                    'Region 4A' => 'Region 4A - CALABARZON', 
                                                    'Region 4B' => 'Region 4B - MIMAROPA', 
                                                    'Region 5' => 'Region 5 - Bicol Region', 
                                                    'Region 6' => 'Region 6 - Western Visayas', 
                                                    'Region 7' => 'Region 7 - Central Visayas', 
                                                    'Region 8' => 'Region 8 - Eastern Visayas', 
                                                    'Region 9' => 'Region 9 - Zamboanga Peninsula', 
                                                    'Region 10' => 'Region 10 - Northern Mindanao', 
                                                    'Region 11' => 'Region 11 - Davao Region', 
                                                    'Region 12' => 'Region 12 - SOCCKSARGEN', 
                                                    'Region 13' => 'Region 13 - Caraga Region'
                                                    ], '',['class' => 'form-control', 'placeholder' => '------------']) }}
                    </div>
                    <div class="form-group">
                        {{Form::label('province', 'Province', ['class' => 'col-form-label'])}}
                        {{Form::text('province', '', ['class' => 'form-control', 'placeholder' => 'Add province'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('municipality', 'City/Municipality', ['class' => 'col-form-label'])}}
                        {{Form::text('municipality', '', ['class' => 'form-control', 'placeholder' => 'Add city or municipality'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('phone', 'Phone Number', ['class' => 'col-form-label'])}}
                        {{Form::text('phone', '', ['class' => 'form-control', 'placeholder' => 'Add phone number'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('fax', 'Fax Number', ['class' => 'col-form-label'])}}
                        {{Form::text('fax', '', ['class' => 'form-control', 'placeholder' => 'Add fax number'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('email', 'Email Address', ['class' => 'col-form-label'])}}
                        {{Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'Add email address'])}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    {{Form::submit('Add Adopter', ['class' => 'btn btn-success'])}}
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
<!-- create adopter end -->

@foreach(App\Adopter::all() as $adopter)
    <!-- edit adopter -->
        <div class="modal fade" id="editAdopterModal-{{$adopter->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {{ Form::open(['action' => ['AdoptersController@editAdopter', $adopter->id], 'method' => 'POST']) }}
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Edit Adopter</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            {{Form::label('name', 'Name', ['class' => 'col-form-label'])}}
                            {{Form::text('name', $adopter->name, ['class' => 'form-control', 'placeholder' => 'Add a name'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('adopter_type', 'Adopter Type')}}
                            <select name="adopter_type" class="form-control" id="adopter_type">
                                <option value=""> Select Adopter Type </option>
                                @foreach(App\AdopterType::all() as $adopterType)
                                    <option value="{{$adopterType->id}}" {{$adopterType->id == $adopter->adopter_type_id ? 'selected' : ''}}>{{$adopterType->name}}</option>
                                @endforeach
                            </select> 
                        </div><div class="form-group">
                            {{Form::label('region', 'Region', ['class' => 'col-form-label'])}}
                            {{Form::select('region', ['ARMM' => 'ARMM - Autonomous Region in Muslim Mindanao', 
                                                        'CAR' => 'CAR - Cordillera Adminisitrative Region', 
                                                        'NCR' => 'NCR - National Capital Region', 
                                                        'Region 1' => 'Region 1 - Ilocos Region', 
                                                        'Region 2' => 'Region 2 - Cagayan Valley', 
                                                        'Region 3' => 'Region 3 - Central Luzon', 
                                                        'Region 4A' => 'Region 4A - CALABARZON', 
                                                        'Region 4B' => 'Region 4B - MIMAROPA', 
                                                        'Region 5' => 'Region 5 - Bicol Region', 
                                                        'Region 6' => 'Region 6 - Western Visayas', 
                                                        'Region 7' => 'Region 7 - Central Visayas', 
                                                        'Region 8' => 'Region 8 - Eastern Visayas', 
                                                        'Region 9' => 'Region 9 - Zamboanga Peninsula', 
                                                        'Region 10' => 'Region 10 - Northern Mindanao', 
                                                        'Region 11' => 'Region 11 - Davao Region', 
                                                        'Region 12' => 'Region 12 - SOCCKSARGEN', 
                                                        'Region 13' => 'Region 13 - Caraga Region'
                                                        ], $adopter->region,['class' => 'form-control', 'placeholder' => '------------']) }}
                        </div>
                        <div class="form-group">
                            {{Form::label('province', 'Province', ['class' => 'col-form-label'])}}
                            {{Form::text('province', $adopter->province, ['class' => 'form-control', 'placeholder' => 'Add province'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('municipality', 'City/Municipality', ['class' => 'col-form-label'])}}
                            {{Form::text('municipality', $adopter->municipality, ['class' => 'form-control', 'placeholder' => 'Add city or municipality'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('phone', 'Phone Number', ['class' => 'col-form-label'])}}
                            {{Form::text('phone', $adopter->phone, ['class' => 'form-control', 'placeholder' => 'Add phone number'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('fax', 'Fax Number', ['class' => 'col-form-label'])}}
                            {{Form::text('fax', $adopter->fax, ['class' => 'form-control', 'placeholder' => 'Add fax number'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('email', 'Email Address', ['class' => 'col-form-label'])}}
                            {{Form::text('email', $adopter->email, ['class' => 'form-control', 'placeholder' => 'Add email address'])}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        {{Form::submit('Save Changes', ['class' => 'btn btn-success'])}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    <!-- edit adopter end -->

    <!-- confirm delete adopter -->
        <div class="modal fade" id="deleteAdopterModal-{{$adopter->id}}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="{{ route('deleteAdopter', $adopter->id) }}" class="deleteForm" method="POST">
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Confirm Delete</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <span>
                            <?php $adops = App\Adopter::with('technologies')->find($adopter->id); ?>
                            Are you sure you want to delete: <b>{{$adopter->name}}</b>?</br></br>
                            @if($adops->technologies->where('approved','=','2')->count() > 0)
                                The following technologies using this adopter will be affected:
                                <ul>
                                    @foreach($adops->technologies as $tech)
                                        <li>{{$tech->title}}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                        <input class="btn btn-danger" type="submit" value="Yes, Delete" onclick="this.disabled=true;this.form.submit();">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    <!-- confirm delete adopter -->
@endforeach
<!-- Adopter End -->
