<!-- confirm delete technology edit -->
@foreach(Victorlap\Approvable\Approval::open()->get() as $editRequest)
@php $type = str_replace("App\\"," ",$editRequest->approvable_type); @endphp
<div class="modal fade" id="editRequestModals-deleteEditRequest-{{$editRequest->id}}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('deleteApproval', $editRequest->id) }}" class="deleteForm" method="POST">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">Confirm Delete</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <span>
                    Are you sure you want to delete EDIT ID: <b>{{$editRequest->id}}</b> 
                    <br>
                    for <b>{{$type}} ID: {{$editRequest->approvable_id}}</b>
                    <br>
                    With the following details: 
                    <br>
                    Field edited: <b>{{$editRequest->key}}</b>
                    <br>
                    Value: <b>{{$editRequest->value}}</b>
                </span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                <input class="btn btn-danger" type="submit" value="Yes, Delete" onclick="this.disabled=true;this.form.submit();">
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
<!-- confirm delete technology edit -->