@extends('layouts.app')
@section('breadcrumb')
    <ol class="breadcrumb pb-0" style="background-color:transparent">
        <li class="breadcrumb-item"><a class="breadcrumb-link" href="https://km4aanr.pcaarrd.dost.gov.ph/">km4aanr</a></li>
        <li class="breadcrumb-item"><a class="breadcrumb-link" href="/">Technology Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Admin Settings</li>
    </ol>
@endsection
@section('content')
@include('admin modals.editRequestModals')
<div class="container-fluid px-0">
    <div class="row mr-0" style="max-height:inherit; min-height:40rem">
        <div class="col-sm-2 bg-dark pr-0" style="min-height:777px">
            <div class="nav nav-tabs" style="border-bottom-width: 0px;">
                <?php 
                    $administrationCount =  $techApprovalCount + $techEditApprovalCount + $generatorsEditApprovalCount +
                                            $applicabilityIndustryApprovalCount + $applicabilityIndustryEditApprovalCount +
                                            $industryApprovalCount + $industryEditApprovalCount + $sectorApprovalCount +
                                            $sectorEditApprovalCount + $commoditiesApprovalCount + $commoditiesEditApprovalCount +
                                            $techCategoriesApprovalCount + $techCategoriesEditApprovalCount + $adopterTypesApprovalCount +
                                            $adopterTypesEditApprovalCount + $adoptersApprovalCount + $adoptersEditApprovalCount +
                                            $agencyApprovalCount + $agencyEditApprovalCount + $generatorsApprovalCount;
                ?>
                <a class="list-group-item active" data-toggle="tab" href="#technology">
                    Technologies 
                    <span class="badge badge-danger" style="{{$techApprovalCount > 0 ? '' : 'display:none'}}">
                        {{$techApprovalCount}}
                    </span>
                </a>
                {{-- <a class="list-group-item" data-toggle="tab" href="#access_levels">
                    Access Levels 
                    <span class="badge badge-danger" style="{{$techApprovalCount > 0 ? '' : 'display:none'}}">
                        {{$techApprovalCount}}
                    </span>
                </a> --}}
                <div id="data-management-accordion" class="w-100">
                    <a class="list-group-item accordion-toggle collapsed" data-toggle="collapse" data-target="#data-management-collapse" aria-expanded="false" href="#">Manage Resources <span class="badge badge-danger" style="{{$administrationCount > 0 ? '' : 'display:none;'}}{{auth()->user()->user_level == 5 ? '' : 'display:none'}}">!</span></a>
                    <div id="data-management-collapse" class="collapse" data-parent="#data-management-accordion">
                        <a class="list-group-item ml-3" data-toggle="tab" href="#applicabilityIndustry">
                            Technology Applicability - Industry 
                            <span class="badge badge-danger" style="{{$applicabilityIndustryApprovalCount + $applicabilityIndustryEditApprovalCount > 0 ? '' : 'display:none;'}}{{auth()->user()->user_level == 5 ? '' : 'display:none'}}">
                                {{$applicabilityIndustryApprovalCount + $applicabilityIndustryEditApprovalCount}}
                            </span>
                        </a>
                        <a class="list-group-item ml-3" data-toggle="tab" href="#industry">
                            Industries 
                            <span class="badge badge-danger" style="{{$industryApprovalCount + $industryEditApprovalCount > 0 ? '' : 'display:none;'}}{{auth()->user()->user_level == 5 ? '' : 'display:none'}}">
                                {{$industryApprovalCount + $industryEditApprovalCount}}
                            </span>
                        </a>
                        <a class="list-group-item ml-3" data-toggle="tab" href="#sector">
                            Sectors 
                            <span class="badge badge-danger" style="{{$sectorApprovalCount + $sectorEditApprovalCount > 0 ? '' : 'display:none;'}}{{auth()->user()->user_level == 5 ? '' : 'display:none'}}">
                                {{$sectorApprovalCount + $sectorEditApprovalCount}}
                            </span>
                        </a>
                        <a class="list-group-item ml-3" data-toggle="tab" href="#commodity">
                            Commodities 
                            <span class="badge badge-danger" style="{{$commoditiesApprovalCount + $commoditiesEditApprovalCount > 0 ? '' : 'display:none;'}}{{auth()->user()->user_level == 5 ? '' : 'display:none'}}">
                                {{$commoditiesApprovalCount + $commoditiesEditApprovalCount}}
                            </span>
                        </a>
                        <a class="list-group-item ml-3" data-toggle="tab" href="#technologyCategory">
                            Technology Categories 
                            <span class="badge badge-danger" style="{{$techCategoriesApprovalCount + $techCategoriesEditApprovalCount > 0 ? '' : 'display:none;'}}{{auth()->user()->user_level == 5 ? '' : 'display:none'}}">
                                {{$techCategoriesApprovalCount + $techCategoriesEditApprovalCount}}
                            </span>
                        </a>
                        <a class="list-group-item ml-3" data-toggle="tab" href="#adopterType">
                            Adopter Types 
                            <span class="badge badge-danger" style="{{$adopterTypesApprovalCount + $adopterTypesEditApprovalCount > 0 ? '' : 'display:none;'}}{{auth()->user()->user_level == 5 ? '' : 'display:none'}}">
                                {{$adopterTypesApprovalCount + $adopterTypesEditApprovalCount}}
                            </span>
                        </a>
                        <a class="list-group-item ml-3" data-toggle="tab" href="#adopter">
                            Adopters 
                            <span class="badge badge-danger" style="{{$adoptersApprovalCount + $adoptersEditApprovalCount > 0 ? '' : 'display:none;'}}{{auth()->user()->user_level == 5 ? '' : 'display:none'}}">
                                {{$adoptersApprovalCount + $adoptersEditApprovalCount}}
                            </span>
                        </a>
                        <a class="list-group-item ml-3" data-toggle="tab" href="#agency">
                            Agencies (Owner) 
                            <span class="badge badge-danger" style="{{$agencyApprovalCount + $agencyEditApprovalCount > 0 ? '' : 'display:none;'}}{{auth()->user()->user_level == 5 ? '' : 'display:none'}}">
                                {{$agencyApprovalCount + $agencyEditApprovalCount}}
                            </span>
                        </a>
                        <a class="list-group-item ml-3" data-toggle="tab" href="#generator">
                            Generators 
                            <span class="badge badge-danger" style="{{$generatorsApprovalCount + $generatorsEditApprovalCount > 0 ? '' : 'display:none;'}}{{auth()->user()->user_level == 5 ? '' : 'display:none'}}">
                                {{$generatorsApprovalCount + $generatorsEditApprovalCount}}
                            </span>
                        </a>
                    </div>
                </div>
                @if(auth()->user()->user_level == 5)
                <div id="manage-users-accordion" class="w-100">
                    <a class="list-group-item accordion-toggle collapsed" data-toggle="collapse" data-target="#manage-users-collapse" aria-expanded="false" href="#">Manage Users</a>
                    <div id="manage-users-collapse" class="collapse" data-parent="#manage-users-accordion">
                        <a class="list-group-item ml-3" data-toggle="tab" href="#manageUsers">Manage Users</a>
                        <a class="list-group-item ml-3" data-toggle="tab" href="#userMessages">User Messages</a>
                    </div>
                </div>
                <div id="manage-page-accordion" class="w-100">
                    <a class="list-group-item accordion-toggle collapsed" data-toggle="collapse" data-target="#manage-page-collapse" aria-expanded="false" href="#">Manage Page</a>
                    <div id="manage-page-collapse" class="collapse" data-parent="#manage-page-accordion">
                        <a class="list-group-item ml-3" href="/admin/manageLandingPage">Manage Landing Page</a>
                    </div>
                </div>
                @endif
                <a class="list-group-item" data-toggle="tab" href="#activity">Activity Logs</a>
            </div>
        </div>
        
        <div class="col-sm-10">
            <div class="mt-3">
                @include('inc.messages')
            </div>
            <div class="tab-content">
                <div class="tab-pane fade active show" id="technology">
                    @include('admin tabs.technologyTab')
                </div>
                
                <div class="tab-pane fade" id="access_levels">
                    @include('admin tabs.accessLevelTab')
                </div>

                <div class="tab-pane fade" id="applicabilityIndustry">
                    @include('admin tabs.applicabilityIndustryTab')
                </div>

                <div class="tab-pane fade" id="industry">
                    @include('admin tabs.industryTab')
                </div>

                <div class="tab-pane fade" id="sector">
                    @include('admin tabs.sectorTab')
                </div>

                <div class="tab-pane fade" id="commodity">
                    @include('admin tabs.commodityTab')
                </div>

                <div class="tab-pane fade" id="technologyCategory">
                    @include('admin tabs.technologyCategoryTab')
                </div>

                <div class="tab-pane fade" id="adopterType">
                    @include('admin tabs.adopterTypeTab')
                </div> 

                <div class="tab-pane fade" id="adopter">
                    @include('admin tabs.adopterTab')
                </div>

                <div class="tab-pane fade" id="agency">
                    @include('admin tabs.agencyTab')
                </div>

                <div class="tab-pane fade" id="generator">
                    @include('admin tabs.generatorTab')
                </div>

                <div class="tab-pane fade" id="activity">
                    @include('admin tabs.activityTab')
                </div>

                <div class="tab-pane fade" id="manageUsers">
                    @include('admin tabs.manageUsersTab')
                </div>

                <div class="tab-pane fade" id="userMessages">
                    @include('admin tabs.userMessagesTab')
                </div>

            </div>
        </div>
    </div>
</div>

<style>
    .status-pill{
        border-radius:24px;
        height:2.5em;
        margin:auto;
        padding-top:.5em;
    }
    .approved-pill{
        background-color:rgb(106,168,78);
    }
    .pending-pill{
        background-color:rgb(241,194,50);
    }
    .inactive-pill{
        background-color:rgb(200,200,200);
    }
    button.chosen {
        outline: 0;
        box-shadow: 0 0 0 0.2rem rgba(52, 144, 220, 0.25);
    }
    .btn-info{
        color: white;
    }
    .list-group-item{
        width:100%;
        border: 0px;
    }
    .accordion-toggle:after {
        /* symbol for "opening" panels */
        font-family: "Font Awesome 5 Free";
        display: inline-block;
        vertical-align: middle;
        font-weight:900;
        content: "\f078";    /* adjust as needed, taken from bootstrap.css */
        float: right;        /* adjust as needed */
        color: white;         /* adjust as needed */
    }
    .accordion-toggle.collapsed:after {
        /* symbol for "collapsed" panels */
        content: "\f054";    /* adjust as needed, taken from bootstrap.css */
    }
</style>

@endsection

@section ('scripts')
<script>
    $(".messageModal").on('shown.bs.modal', function (e) {
        var number = $(this).attr('id').split('-').pop();
        $.ajax({
            type: "POST",
            url: '/message/read/'+number,
            data: {_token: '{{csrf_token()}}' },
        });
        $("#badge-"+number).replaceWith("<span class='badge badge-secondary' style='font-size:15px'>Read</span>");
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // gets activated tab
        var target = $(e.target).attr("href");
        // remove active from all other menu items
        $(".nav a").removeClass("active");
        // set active for current menu item
        $('a[href="' + target + '"]').addClass('active');
        $('a[href="' + target + '"]').parent('div').addClass('show');
        $('a[href="' + target + '"]').parent('div').prev().removeClass('collapsed');
    });
    $(".list-group-item-action").on('click', function() {
        $(".list-group-item-action").each(function(index) {
            $(this).removeClass("active show");
        });
    })
    $(function() {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            localStorage.setItem('lastTab', $(this).attr('href'));
        });
        var lastTab = localStorage.getItem('lastTab');
        if (lastTab) {
            $('[href="' + lastTab + '"]').tab('show');
        }
    });
    $(document).ready(function(){
        $('body').on('click', '.list-group a', function (e) {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            
            //do any other button related things
        });

        $('.dynamic_select').change(function(){
            if($(this).val() != ''){
                var select = $(this).attr('id');
                var select = select+'_id';
                var value = $(this).val();
                var dependent = $(this).data('dependent');
                var identifier = $(this).data('identifier');
                console.log(identifier);
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('fetchDependent') }}",
                    method:"POST",
                    data:{select:select, value:value, _token:_token, dependent:dependent},
                    success:function(result){
                        $('#'+dependent+'-edit-'+identifier).html(result);
                    }
                })
            }
        });
       
        $('.dynamic_create').change(function(){
            if($(this).val() != ''){
                var select = $(this).attr('id');
                var select = select+'_id';
                var value = $(this).val();
                var dependent = $(this).data('dependent');
                var identifier = $(this).data('identifier');
                console.log(identifier);
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('fetchDependent') }}",
                    method:"POST",
                    data:{select:select, value:value, _token:_token, dependent:dependent},
                    success:function(result){
                        $('#sector-create').html(result);
                    }
                })
            }
        });

        $('.table').DataTable();

        $('#logs_table').DataTable({
            order: [[0, 'desc']],
        });

        $tech_table = $('#tech_table').DataTable();

    });
</script>
@endsection