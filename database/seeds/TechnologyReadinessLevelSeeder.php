<?php

use Illuminate\Database\Seeder;

class TechnologyReadinessLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('technology_readiness')->insert([
            'level' => 1,
            'title' => "Basic principles observed and reported",
        ]);
        DB::table('technology_readiness')->insert([
            'level' => 2,
            'title' => "Technology concept and/or application formulated",
        ]);
        DB::table('technology_readiness')->insert([
            'level' => 3,
            'title' => "Analytical and experimental critical function and/or characteristic proof-of-concept",
        ]);
        DB::table('technology_readiness')->insert([
            'level' => 4,
            'title' => "Component and/or breadboard validation in laboratory environment",
        ]);
        DB::table('technology_readiness')->insert([
            'level' => 5,
            'title' => "Component and/or breadboard validation in relevant environment",
        ]);
        DB::table('technology_readiness')->insert([
            'level' => 6,
            'title' => "System/subsystem model or prototype demonstration in a relevant environment (ground or space)",
        ]);
        DB::table('technology_readiness')->insert([
            'level' => 7,
            'title' => "System prototype demonstration in a space environment",
        ]);
        DB::table('technology_readiness')->insert([
            'level' => 8,
            'title' => "Actual system completed and 'flight qualified' through test and demonstration (ground or space)",
        ]);
        DB::table('technology_readiness')->insert([
            'level' => 9,
            'title' => "Actual system 'flight proven' through successful mission operations",
        ]);
    }
}
