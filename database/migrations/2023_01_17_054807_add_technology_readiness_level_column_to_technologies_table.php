<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTechnologyReadinessLevelColumnToTechnologiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('technologies', function (Blueprint $table) {
            $table->unsignedBigInteger('technology_readiness_id')->nullable();
            $table->foreign('technology_readiness_id')->references('id')->on('technology_readiness')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('technologies', function (Blueprint $table) {
            $table->dropForeign(['technology_readiness_id']);
            $table->dropColumn('technology_readiness_id');
        });
    }
}
