<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIconAndDisabledFieldToSocialMediaSticky extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('social_media_sticky', function (Blueprint $table) {
            //
            $table->boolean('disabled')->default(0);
            $table->string('thumbnail')->nullable();
            $table->mediumText('description')->nullable();
            $table->string('bgcolor')->default("#FFFFFF");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('social_media_sticky', function (Blueprint $table) {
            //
            $table->dropColumn('disabled');
            $table->dropColumn('thumbnail');
            $table->dropColumn('description');
            $table->dropColumn('bgcolor');
        });
    }
}
