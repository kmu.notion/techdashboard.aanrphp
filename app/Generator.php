<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Victorlap\Approvable\Approvable;

class Generator extends Model
{
    use Approvable;
    
    public function agency()
    {
        return $this->belongsTo(Agency::class);
    }

    public function technologies()
    {
        return $this->belongsToMany(Technology::class);
    }
}
