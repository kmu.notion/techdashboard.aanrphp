<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Victorlap\Approvable\Approvable;

class Agency extends Model
{
    use Approvable;
    
    public function generator()
    {
        return $this->hasMany(Generator::class);
    }

    public function technologies()
    {
        return $this->belongsToMany(Technology::class);
    }
}
