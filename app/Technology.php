<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Victorlap\Approvable\Approvable;

class Technology extends Model
{
    use Approvable;
    
    protected $table = 'technologies';
    protected $primaryKey = 'id';
    protected $fillable = ['title', 'description', 'significance', 'target_users', 'approved'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function patents()
    {
        return $this->hasMany(Patent::class);
    }

    public function r_d_results()
    {
        return $this->hasMany(RDResult::class);
    }

    public function utility_models()
    {
        return $this->hasMany(UtilityModel::class);
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function industrial_designs()
    {
        return $this->hasMany(IndustrialDesign::class);
    }

    public function copyrights()
    {
        return $this->hasMany(Copyright::class);
    }

    public function trademarks()
    {
        return $this->hasMany(Trademark::class);
    }

    public function plant_variety_protections()
    {
        return $this->hasMany(PlantVarietyProtection::class);
    }

    public function applicability_industries()
    {
        return $this->belongsToMany(ApplicabilityIndustry::class);
    }

    public function commodities()
    {
        return $this->belongsToMany(Commodity::class, 'commodity_technology');
    }

    public function technology_categories()
    {
        return $this->belongsToMany(TechnologyCategory::class);
    }

    public function agencies()
    {
        return $this->belongsToMany(Agency::class);
    }

    public function generators()
    {
        return $this->belongsToMany(Generator::class);
    }

    public function potential_adopters()
    {
        return $this->belongsToMany(PotentialAdopter::class);
    }

    public function adopters()
    {
        return $this->belongsToMany(Adopter::class);
    }

    public function readiness_level()
    {
        return $this->belongsTo(TechnologyReadiness::class);
    }
}
