<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Victorlap\Approvable\Approvable;

class Commodity extends Model
{
    use Approvable;

    public function sector()
    {
        return $this->belongsTo(Sector::class);
    }

    public function technologies()
    {
        return $this->belongsToMany(Technology::class, 'commodity_technology');
    }

}
    