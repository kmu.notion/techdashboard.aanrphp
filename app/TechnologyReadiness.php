<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechnologyReadiness extends Model
{
    //
    protected $table = 'technology_readiness';
    
    public function technologies()
    {
        return $this->hasMany(Technology::class);
    }
}
