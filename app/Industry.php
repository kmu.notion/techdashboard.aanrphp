<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Victorlap\Approvable\Approvable;

class Industry extends Model
{
    use Approvable;
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;

    public function sectors()
    {
        return $this->hasMany(Sector::class);
    }

    public function commodities()
    {
        return $this->hasManyThrough(Commodity::class, Sector::class);
    }

    public function technologies()
    {   
        return $this->hasManyDeep(Technology::class, [Sector::class, Commodity::class, 'commodity_technology']);
    }
}
