<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Victorlap\Approvable\Approvable;

class Sector extends Model
{
    use Approvable;

    public function commodities()
    {
        return $this->hasMany(Commodity::class);
    }

    public function industry()
    {
        return $this->belongsTo(Industry::class);
    }
}
