<?php

namespace App\Http\Controllers;

use App\Technology;
use Illuminate\Http\Request;
use DB;

class ApprovablesController extends Controller
{
    public function delete($approval_id)
    {
        $approval = DB::table('approvals')->where('id', $approval_id);

        if ($approval == null) {
            return redirect()->back()->with('error', 'Cannot find edit request.');
        }

        $approval->delete();

        return redirect()->back()->with('success', 'Edit request #'.$approval_id.' deleted.');
    }

    public function approve($approval_id)
    {
        $resource = DB::table('approvals')->find($approval_id);

        if ($resource == null) {
            return redirect()->back()->with('error', 'Cannot find edit request.');
        }

        $approvals = \Victorlap\Approvable\Approval::open()->ofClass($resource->approvable_type)->get();

        if ($approvals == null) {
            return redirect()->back()->with('error', 'Cannot find edit request.');
        }

        $approval = $approvals->find($approval_id);

        if ($approval == null) {
            return redirect()->back()->with('error', 'Cannot find edit request.');
        }

        $approval->accept();

        return redirect()->back()->with('success', 'Edit request #'.$approval_id.' approved.');
    }

    public function reject($approval_id)
    {
        $resource = DB::table('approvals')->find($approval_id);

        if ($resource == null) {
            return redirect()->back()->with('error', 'Cannot find edit request.');
        }

        $approvals = \Victorlap\Approvable\Approval::open()->ofClass($resource->approvable_type)->get();

        if ($approvals == null) {
            return redirect()->back()->with('error', 'Cannot find edit request.');
        }

        $approval = $approvals->find($approval_id);

        if ($approvals == null) {
            return redirect()->back()->with('error', 'Cannot find edit request.');
        }

        $approval->reject();

        return redirect()->back()->with('success', 'Edit request #'.$approval_id.' rejected.');
    }
}
