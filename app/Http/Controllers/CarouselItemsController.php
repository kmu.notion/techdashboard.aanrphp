<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CarouselItem;
use App\Log;

class CarouselItemsController extends Controller
{
    public function add(Request $request){

        $this->validate($request, [
            'title' => 'required|max:120',
            'image' => 'required|max:10240',
            'position' => 'required|integer',
        ]);

        $new = new CarouselItem;

        if($request->hasFile('image')){
            if($new->banner != null){
                $image_path = public_path().'/storage/page_images/'.$page->banner;
                unlink($image_path);
            }
            $imageFile = $request->file('image');
            $imageName = uniqid().$imageFile->getClientOriginalName();
    		$imageFile->move(public_path('/storage/page_images/'), $imageName);
        }

        if($request->position <= CarouselItem::count()) {
            foreach(CarouselItem::where('position', '>=', $request->position)->get() as $item) {
                $item->position = $item->position + 1;
                $item->save();
            }  
        }

        $new->title = $request->title;
        $new->subtitle = $request->subtitle;
        $new->button_link = $request->button_link;
        $new->position = $request->position;
        $new->banner = $imageName;
        $new->save();

        $user = auth()->user();
        $log = new Log;
        $log->user_id = $user->id;
        $log->user_name = $user->name;
        $log->user_level = $user->user_level;
        $log->action = 'Added an entry.';
        $log->IP_address = $request->ip();
        $log->resource = 'Carousel Items';
        $log->save();

        return redirect('/admin/manageLandingPage')->with('success', 'Carousel item added');
    }

    public function edit(Request $request, $slider_id) {
        $this->validate($request, [
            'title' => 'required|max:120',
            'position' => 'required|integer',
        ]);
        
        $slider = CarouselItem::find($slider_id);
        
        if($slider == null) {
            return redirect('/admin/manageLandingPage')->with('error', 'Carousel item not found.');
        }

        $slider->title = $request->title;
        $slider->subtitle = $request->subtitle;
        $slider->button_link = $request->button_link;
        
        if($slider->position > $request->position) {
            foreach(CarouselItem::havingBetween('position', [$request->position, $slider->position-1])->get() as $item){
                $item->position = $item->position + 1;
                $item->save();
            }
        }

        if($slider->position < $request->position) {
            foreach(CarouselItem::havingBetween('position', [$slider->position+1, $request->position])->get() as $item){
                $item->position = $item->position - 1;
                $item->save();
            }
        }

        $slider->position = $request->position;

        if($request->hasFile('image')){
            if($slider->banner != null){
                $image_path = public_path().'/storage/page_images/'.$slider->banner;

                if(file_exists($image_path)) {
                    unlink($image_path);
                }
            }
            $imageFile = $request->file('image');
            $imageName = uniqid().$imageFile->getClientOriginalName();
    		$imageFile->move(public_path('/storage/page_images/'), $imageName);
            $slider->banner = $imageName;
        }
        
        $slider->save();

        return redirect('/admin/manageLandingPage')->with('success', 'Carousel item edited');
    }

    public function delete($slider_id){
        $slider = CarouselItem::find($slider_id);

        if($slider == null) {
            return redirect('/admin/manageLandingPage')->with('error', 'Carousel item not found.');
        }

        if($slider->banner != null){
            $image_path = public_path().'/storage/page_images/'.$slider->banner;
            if(file_exists($image_path)){
                    unlink($image_path);
                }
        }

        foreach(CarouselItem::where('position', '>', $slider->position)->get() as $item) {
            $item->position = $item->position - 1;
            $item->save();
        }

        $slider->delete();
        
        return redirect()->back()->with('success','Carousel Slider Deleted.'); 
    }
}
