<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HeaderLink;
use App\Log;

class HeaderLinksController extends Controller
{
    public function addHeaderLink(Request $request){
        $this->validate($request, [
            'name' => 'required|max:10240',
            'link' => 'required',
            'position' => 'required'
        ]);

        if($request->position <= HeaderLink::count()) {
            foreach(HeaderLink::where('position', '>=', $request->position)->get() as $link) {
                $link->position = $link->position + 1;
                $link->save();
            }  
        }

        $item = new HeaderLink;
        $item->name = $request->name;
        $item->link = $request->link;
        $item->position = $request->position;
        $item->save();
        $user = auth()->user();
        $log = new Log;
        $log->user_id = $user->id;
        $log->user_name = $user->name;
        $log->user_level = $user->user_level;
        $log->action = 'Added a header link.';
        $log->IP_address = $request->ip();
        $log->resource = 'Header';
        $log->save();

        return redirect('/admin/manageLandingPage')->with('success', 'Header Link added');
    }

    public function editHeaderLink(Request $request, $header_id){
        $this->validate($request, array(
            'name' => 'required|max:50',
            'position' => 'required'
        ));
        
        $header = HeaderLink::find($header_id);

        if($header == null) {
            return redirect('/admin/manageLandingPage')->with('error', 'Header link not found.');
        }
        if($header->position > $request->position) {
            foreach(HeaderLink::havingBetween('position', [$request->position, $header->position-1])->get() as $item){
                $item->position = $item->position + 1;
                $item->save();
            }
        }
        if($header->position < $request->position) {
            foreach(HeaderLink::havingBetween('position', [$header->position+1, $request->position])->get() as $item){
                $item->position = $item->position - 1;
                $item->save();
            }
        }

        $header->name = $request->name;
        $header->position = $request->position;
        $header->link = $request->link;
        $header->save();
        $user = auth()->user();
        $log = new Log;
        $log->user_id = $user->id;
        $log->user_name = $user->name;
        $log->user_level = $user->user_level;
        $log->action = 'Edited a header link.';
        $log->IP_address = $request->ip();
        $log->resource = 'Header';
        $log->save();

        return redirect()->back()->with('success','Header Link Updated.'); 
    }

    public function deleteHeaderLink($header_id){
        $header = HeaderLink::find($header_id);

        if($header == null) {
            return redirect('/admin/manageLandingPage')->with('error', 'Header link not found.');
        }
        foreach(HeaderLink::where('position', '>', $header->position)->get() as $item) {
            $item->position = $item->position - 1;
            $item->save();
        }

        $header->delete();
        return redirect()->back()->with('success','Header Link Deleted.'); 
    }
}
