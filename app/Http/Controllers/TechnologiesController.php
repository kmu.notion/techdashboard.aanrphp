<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Technology;
use Victorlap\Approvable\Approvable;
use Kyslik\ColumnSortable\Sortable;
use App\Log;
use Illuminate\Support\Facades\DB;

class TechnologiesController extends Controller
{
    public function addTechnology(Request $request)
    {
        $this->validate($request, array(
            'title' => 'required|max:255',
            'technology_categories' => 'required',
            'applicability_location' => 'required',
            'applicability_industries' => 'required',
            'commodities' => 'required',
            'technology_categories' => 'required',
        ));

        $tech = new Technology();
        $tech->title = $request->title;
        $tech->description = $request->description;
        $tech->significance = $request->significance;
        $tech->target_users = $request->target_users;
        $tech->technology_readiness_id = $request->technology_readiness_id;
        $tech->applicability_location = $request->applicability_location;
        $user = auth()->user();
        $tech->user_id = $user->id;
        $tech->save();

        $log = new Log();
        $log->user_id = $user->id;
        $log->user_name = $user->name;
        $log->user_level = $user->user_level;
        $log->action = 'Added a new technology';
        $log->IP_address = $request->ip();
        $log->resource = 'Technologies';
        $log->save();

        $tech->applicability_industries()->sync($request->applicability_industries);
        $tech->technology_categories()->sync($request->technology_categories);
        $tech->commodities()->sync($request->commodities);
        $mess = 'Technology Added. <a href="/admin/tech/'.$tech->id.'/edit">Click this link add more details.</a>';

        if ($user->user_level == 4) {
            $mess = 'Technology added. Click publish to request for site admin approval or <a href="/admin/tech/'.$tech->id.'/edit">click this link add more details.</a>.';
        }

        return redirect('/admin?showTechnologies=user')->with('success', $mess);
    }

    public function editTechnology(Request $request, $tech_id)
    {
        $this->validate($request, array(
            'title' => 'required|max:255',
            'technology_categories' => 'required',
            'applicability_location' => 'required',
            'applicability_industries' => 'required',
            'commodities' => 'required',
            'technology_categories' => 'required',
        ));

        $mess = '';
        $tech = Technology::find($tech_id);

        if ($tech == null) {
            return redirect()->back()->with('error', 'Cannot edit. Technology was already deleted by its creator.');
        }

        if ($tech->title != $request->title || $tech->description != $request->description
            || $tech->significance != $request->significance || $tech->target_users != $request->target_users
            || $tech->applicability_location != $request->applicability_location || $tech->year_developed != $request->year_developed
            || $tech->commercialization_mode != $request->commercialization_mode) {
            $mess .= 'Basic Information Updated. </br>';
        }

        $tech->title = $request->title;
        $tech->description = $request->description;
        $tech->significance = $request->significance;
        $tech->target_users = $request->target_users;
        $tech->applicability_location = $request->applicability_location;
        $tech->year_developed = $request->year_developed;
        $tech->commercialization_mode = $request->commercialization_mode;
        $tech->technology_readiness_id = $request->technology_readiness_id;
        $user = auth()->user();
        $tech->user_id = $user->id;

        if ($request->hasFile('banner')) {
            if ($tech->banner != null) {
                $image_path = public_path().'/storage/page_images/'.$tech->banner;
                unlink($image_path);
            }
            $imageFile = $request->file('banner');
            $imageName = uniqid().$imageFile->getClientOriginalName();
            $imageFile->move(public_path('/storage/page_images/'), $imageName);
            $tech->banner = $imageName;
        }

        if ($tech->basic_research_title != $request->basic_research_title
            || $tech->basic_research_funding != $request->basic_research_funding
            || $tech->basic_research_implementing != $request->basic_research_implementing
            || $tech->basic_research_cost != str_replace(',', '', $request->basic_research_cost)
            || $tech->basic_research_start_date != $request->basic_research_start_date
            || $tech->basic_research_end_date != $request->basic_research_end_date
            || $tech->applied_research_type != $request->applied_research_type
            || $tech->applied_research_title != $request->applied_research_title
            || $tech->applied_research_funding != $request->applied_research_funding
            || $tech->applied_research_implementing != $request->applied_research_implementing
            || $tech->applied_research_cost != str_replace(',', '', $request->applied_research_cost)
            || $tech->applied_research_start_date != $request->applied_research_start_date
            || $tech->applied_research_end_date != $request->applied_research_end_date) {
            $mess .= 'Technology Background Updated. </br>';
        }

        $tech->basic_research_title = $request->basic_research_title;
        $tech->basic_research_funding = $request->basic_research_funding;
        $tech->basic_research_implementing = $request->basic_research_implementing;
        $tech->basic_research_cost = str_replace(',', '', $request->basic_research_cost);
        $tech->basic_research_start_date = $request->basic_research_start_date;
        $tech->basic_research_end_date = $request->basic_research_end_date;
        $tech->applied_research_type = $request->applied_research_type;
        $tech->applied_research_title = $request->applied_research_title;
        $tech->applied_research_funding = $request->applied_research_funding;
        $tech->applied_research_implementing = $request->applied_research_implementing;
        $tech->applied_research_cost = str_replace(',', '', $request->applied_research_cost);
        $tech->applied_research_start_date = $request->applied_research_start_date;
        $tech->applied_research_end_date = $request->applied_research_end_date;

        if ($tech->application_of_technology != $request->application_of_technology
            || $tech->limitation_of_technology != $request->limitation_of_technology) {
            $mess .= 'Other Details Updated. </br>';
        }

        $tech->application_of_technology = $request->application_of_technology;
        $tech->limitation_of_technology = $request->limitation_of_technology;
        $tech->save();

        if ($tech->approved != 2 || $user->user_level == 5) {
            $approvals = $tech->approvals()->open()->get();
            $approvals->each->accept();
        }

        $log = new Log();
        $log->user_id = $user->id;
        $log->user_name = $user->name;
        $log->user_level = $user->user_level;
        $log->action = 'Changed \''. $request->name.'\' details';
        $log->IP_address = $request->ip();
        $log->resource = 'Technologies';
        $log->save();

        $tech->applicability_industries()->sync($request->applicability_industries);
        $tech->technology_categories()->sync($request->technology_categories);
        $tech->commodities()->sync($request->commodities);
        $tech->generators()->sync($request->generators);
        $tech->agencies()->sync($request->owners);
        $tech->adopters()->sync($request->adopters);

        if ($user->user_level == 4) {
            $mess .= ' Technology update request sent to site admin for approval.';
        }

        return redirect('/admin?showTechnologies=user')->with('success', $mess);
    }

    public function deleteTechnology($tech_id, Request $request)
    {
        $tech = Technology::find($tech_id);

        if ($tech == null) {
            return redirect()->back()->with('error', 'Cannot delete. Technology was already deleted by its creator.');
        }

        if ($tech->banner != null) {
            $image_path = public_path().'/storage/page_images/'.$tech->banner;

            if (file_exists($image_path)) {
                unlink($image_path);
            }
        }

        $tech->approvals()->whereNull('approved')->delete();
        $tech->technology_categories()->detach();
        $tech->applicability_industries()->detach();
        $tech->commodities()->detach();
        $tech->generators()->detach();
        $tech->agencies()->detach();
        $tech->adopters()->detach();
        $log = new Log();
        $user = auth()->user();
        $log->action = 'Removed '.$tech->name.' from Technologies';
        $tech->delete();
        $log->user_id = $user->id;
        $log->user_name = $user->name;
        $log->user_level = $user->user_level;
        $log->IP_address = $request->ip();
        $log->resource = 'Technologies';
        $log->save();

        return redirect()->back()->with('success', 'Technology Deleted.');
    }

    public function approveTechnology($tech_id)
    {
        $tech = Technology::find($tech_id);

        if ($tech == null) {
            return redirect()->back()->with('error', 'Cannot approve. Technology was already deleted by its creator.');
        }

        if ($tech->approved != 2) {
            $tech->approved = 2;
            $tech->save();
        }

        $approvals = $tech->approvals()->open()->get();
        $approvals->each->accept();

        return redirect()->back()->with('success', 'Technology Approved.');
    }

    public function rejectTechnology($tech_id)
    {
        $tech = Technology::find($tech_id);

        if ($tech == null) {
            return redirect()->back()->with('error', 'Cannot reject. Technology was already deleted by its creator.');
        }

        if ($tech->approved != 2) {
            $tech->approved = 0;
            $tech->save();
        }

        $approvals = $tech->approvals()->open()->get();
        $approvals->each->reject();

        return redirect()->back()->with('success', 'Technology Rejected.');
    }

    public function togglePublishTechnology($tech_id, Request $request)
    {
        $tech = Technology::find($tech_id);
        $user = auth()->user();

        if ($tech->approved == 0) {
            $tech->approved = 1;
            $message = 'Technology submitted for approval.';

            if ($user->user_level == 5) {
                $tech->approved = 2;
                $message = 'Technology added.';
            }
        } elseif ($tech->approved == 1 || $tech->approved == 2) {
            $tech->approved = 0;
            $message = 'Technology unpublished.';
        }

        $tech->save();
        $approvals = $tech->approvals()->open()->get();
        $approvals->each->accept();

        return redirect()->back()->with('success', $message);
    }

    public function toggleIsTradeSecret($tech_id, Request $request)
    {
        $tech = Technology::find($tech_id);
        $tech->is_trade_secret = $request->input('is_trade_secret');
        $tech->save();

        $user = auth()->user();
        $log = new Log();
        $log->user_id = $user->id;
        $log->user_name = $user->name;
        $log->user_level = $user->user_level;
        $log->action = 'Toggle Trade Secret from'. $tech->title;
        $log->IP_address = $request->ip();
        $log->resource = 'Technologies';
        $log->save();


        if ($tech->approved != 2 || $user->user_level == 5) {
            $approvals = $tech->approvals()->open()->get();
            $approvals->each->accept();
            return redirect()->back()->with('success', 'Technology Protection Type Updated.');
        } else {
            return redirect()->back()->with('success', 'Technology Protection Type is submitted for approval.');
        }
    }

    public function toggleIsInvention($tech_id, Request $request)
    {
        $tech = Technology::find($tech_id);
        $tech->is_invention = $request->input('is_invention');
        $tech->save();

        $user = auth()->user();
        $log = new Log();
        $log->user_id = $user->id;
        $log->user_name = $user->name;
        $log->user_level = $user->user_level;
        $log->action = 'Toggle Invention from'. $tech->title;
        $log->IP_address = $request->ip();
        $log->resource = 'Technologies';
        $log->save();


        if ($tech->approved != 2 || $user->user_level == 5) {
            $approvals = $tech->approvals()->open()->get();
            $approvals->each->accept();
            return redirect()->back()->with('success', 'Technology Protection Type Updated.');
        } else {
            return redirect()->back()->with('success', 'Technology Protection Type Update is submitted for approval.');
        }
    }
}
