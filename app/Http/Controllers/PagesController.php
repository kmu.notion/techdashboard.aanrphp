<?php

namespace App\Http\Controllers;

use Request;
use App\Industry;
use App\Technology;
use App\TechnologyCategory;
use App\AdopterType;
use App\Agency;
use App\Adopter;
use App\Commodity;
use App\Generator;
use App\Sector;
use App\Patent;
use App\CarouselItem;
use App\LandingPage;
use App\ApplicabilityIndustry;
use App\HeaderLink;
use App\UserMessage;
use App\RDResult;
use App\SocialMediaSticky;
use App\TechnologyReadiness;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Victorlap\Approvable\Approvable;
use Illuminate\Support\Facades\Input;
use Auth;
use Redirect;

class PagesController extends Controller
{
    use Approvable;

    public function index()
    {
        return view('pages.index', [
            'technologies' => Technology::with([
                    'agencies',
                    'technology_categories'
                ])->get(),
            'generators' => Generator::with([ 'agency' ])->get(),
            'techGrid' => Technology::where('approved', '=', '2')->with('agencies'),
            'headerLinks' => HeaderLink::get(),
            'userMessages' => UserMessage::get(),
            'applicabilityIndustries' => ApplicabilityIndustry::where('approved', '=', '2')->get(),
            'adopter_technologies' => DB::table('adopter_technology')->get(),
            'commodity_technologies' => DB::table('commodity_technology')->get(),
            'rd_results' => RDResult::get(),
            'landing_page' => LandingPage::first(),
            'carousel_items' => CarouselItem::orderBy('position')->get(),
            'stickies' => SocialMediaSticky::where('disabled', 0)->get(),
            'industries' => Industry::get(),
            'sectors' => Sector::get(),
            'commodities' => Commodity::get(),
            'agencies' => Agency::get(),
            'technology_technology_categories' => DB::table('technology_technology_category')->get(),
            'agency_technologies' => DB::table('agency_technology')->get(),
            'technology_categories' => DB::table('technology_categories')->get(),
            'adopters' => Adopter::get(),
        ]);
    }

    public function search()
    {
        $variables = [
            'technologies' => Technology::with([
                'agencies',
                'technology_categories'
            ])->get(),
            'generators' => Generator::with([ 'agency' ])->get(),
            'techGrid' => Technology::where('approved', '=', '2')->with('agencies'),
            'headerLinks' => HeaderLink::get(),
            'userMessages' => UserMessage::get(),
            'applicabilityIndustries' => ApplicabilityIndustry::where('approved', '=', '2')->get(),
            'adopter_technologies' => DB::table('adopter_technology')->get(),
            'commodity_technologies' => DB::table('commodity_technology')->get(),
            'rd_results' => RDResult::get(),
            'landing_page' => LandingPage::first(),
            'carousel_items' => CarouselItem::orderBy('position')->get(),
            'stickies' => SocialMediaSticky::where('disabled', 0)->get(),
            'industries' => Industry::get(),
            'sectors' => Sector::get(),
            'commodities' => Commodity::get(),
            'agencies' => Agency::get(),
            'technology_technology_categories' => DB::table('technology_technology_category')->get(),
            'agency_technologies' => DB::table('agency_technology')->get(),
            'technology_categories' => DB::table('technology_categories')->get(),
            'adopters' => Adopter::get(),
            ];
        if (Request::get('searchForm') == '') {
            return Redirect::route('pages.index', $variables)->with('error', 'Please enter a query.');
        }

        $query = Request::get('searchForm');
        $start = Request::get('start');
        $end = Request::get('end');
        $results = Technology::where('approved', '=', '2')->where('title', 'LIKE', '%'.$query.'%')->paginate(6);

        if ($start && $end) {
            $results = Technology::where('approved', '=', '2')->where('title', 'LIKE', '%'.$query.'%')->whereBetween('year_developed', array($start, $end))->paginate(6);
        }

        if (count($results) == 0) {
            return view('pages.index', $variables)->withMessage('No Technology found. Try to search again!');
        }

        return view('pages.index', array_push($variables, 'results', $results));
    }

    public function contactUsPage()
    {
        return view('pages.contactUs', [ 'technologies' => Technology::where('approved', '=', '2')->pluck('title', 'id')->all() ]);
    }

    public function getAdmin()
    {
        if(!Auth::check()) {
            return Redirect::route('login')->with('error', 'You have to be logged in to access this page.');
        }

        if(Auth::user()->user_level < 4) {
            return redirect()->back()->with('error', 'Your user level doesnt have access this page.');
        }

        return view('pages.admin', [
            'industries' => Industry::pluck('name', 'id')->all(),
            'sectors' => Sector::pluck('name', 'id')->all(),
            'commodities' => Commodity::pluck('name', 'id')->all(),
            'adopterTypes' => AdopterType::pluck('name', 'id')->all(),
            'technologyCategories' => TechnologyCategory::pluck('name', 'id')->all(),
            'agencies' => Agency::pluck('name', 'id')->all(),
            'applicabilityIndustries' => ApplicabilityIndustry::where('approved', '=', '2')->get(),
            'techApprovalCount' => Technology::all()->where('approved', '==', '1')->count(),
            'techEditApprovalCount' => \Victorlap\Approvable\Approval::open()->ofClass(Technology::class)->get()->count(),
            'applicabilityIndustryApprovalCount' => ApplicabilityIndustry::all()->where('approved', '==', '1')->count(),
            'applicabilityIndustryEditApprovalCount' => \Victorlap\Approvable\Approval::open()->ofClass(ApplicabilityIndustry::class)->get()->count(),
            'industryApprovalCount' => Industry::all()->where('approved', '==', '1')->count(),
            'industryEditApprovalCount' => \Victorlap\Approvable\Approval::open()->ofClass(Industry::class)->get()->count(),
            'sectorApprovalCount' => Sector::all()->where('approved', '==', '1')->count(),
            'sectorEditApprovalCount' => \Victorlap\Approvable\Approval::open()->ofClass(Sector::class)->get()->count(),
            'commoditiesApprovalCount' => Commodity::all()->where('approved', '==', '1')->count(),
            'commoditiesEditApprovalCount' => \Victorlap\Approvable\Approval::open()->ofClass(Commodity::class)->get()->count(),
            'techCategoriesApprovalCount' => TechnologyCategory::all()->where('approved', '==', '1')->count(),
            'techCategoriesEditApprovalCount' => \Victorlap\Approvable\Approval::open()->ofClass(TechnologyCategory::class)->get()->count(),
            'adopterTypesApprovalCount' => AdopterType::all()->where('approved', '==', '1')->count(),
            'adopterTypesEditApprovalCount' => \Victorlap\Approvable\Approval::open()->ofClass(AdopterType::class)->get()->count(),
            'adoptersApprovalCount' => Adopter::all()->where('approved', '==', '1')->count(),
            'adoptersEditApprovalCount' => \Victorlap\Approvable\Approval::open()->ofClass(Adopter::class)->get()->count(),
            'agencyApprovalCount' => Agency::all()->where('approved', '==', '1')->count(),
            'agencyEditApprovalCount' => \Victorlap\Approvable\Approval::open()->ofClass(Agency::class)->get()->count(),
            'generatorsApprovalCount' => Generator::all()->where('approved', '==', '1')->count(),
            'generatorsEditApprovalCount' => \Victorlap\Approvable\Approval::open()->ofClass(Generator::class)->get()->count(),
        ]);
    }

    public function fetchDependent(Request $request)
    {
        $output = '<option value="">Select '.ucfirst($request->get('dependent')).'</option>';

        foreach (Sector::all()->where($request->get('select'), $request->get('value')) as $row) {
            $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
        }

        echo $output;
    }

    public function techEditPage($id)
    {
        $tech = Technology::find($id);

        if ($tech == null) {
            return redirect()->back()->with('error', 'Cannot edit. Technology was already deleted by its creator.');
        }

        return view('pages.techEdit', [
            'industries' => Industry::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'sectors' => Sector::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'commodities' => Commodity::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'applicabilityIndustries' => ApplicabilityIndustry::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'generators' => Generator::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'adopterTypes' => AdopterType::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'technologyCategories' => TechnologyCategory::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'agencies' => Agency::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'adopters' => Adopter::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'patents' => Patent::pluck('patent_number', 'id')->all(),
            'tech' => $tech,
            'trl' => TechnologyReadiness::select(DB::raw("CONCAT(level,' - ',title) AS trl"), 'id')->get(),
        ]);
    }

    public function techAddPage()
    {
        return view('pages.techAdd', [
            'industries' => Industry::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'sectors' => Sector::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'commodities' => Commodity::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'adopterTypes' => AdopterType::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'technologyCategories' => TechnologyCategory::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'applicabilityIndustries' => ApplicabilityIndustry::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'agencies' => Agency::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'generators' => Generator::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'adopters' => Adopter::where('approved', '=', '2')->pluck('name', 'id')->all(),
            'trl' => TechnologyReadiness::select(DB::raw("CONCAT(level,' - ',title) AS trl"), 'id')->get(),
        ]);
    }

    public function getJSON()
    {
        return view('api.current', [
            'technologies' => Technology::all(),
            'technologyCategories' => TechnologyCategory::pluck('name', 'id')->all(),
            'industries' => Industry::pluck('name', 'id')->all(),
            'sectors' => Sector::pluck('name', 'id')->all(),
            'commodities' => Commodity::all(),
            'agencies' => Agency::pluck('name', 'id')->all(),
            'generators' => Generator::pluck('name', 'id')->all(),
        ]);
    }

    public function manageLandingPage()
    {
        return view('pages.manage', [
            'carousel_items' => CarouselItem::get(),
            'landing_page' => LandingPage::find(1),
            'header_links' => HeaderLink::all(),
        ]);
    }

    public function printTechPDF($tech_id)
    {
        $tech = Technology::where('id', '=', $tech_id)->first();
        $pdf = PDF::loadView('pages.techPDF', ['tech' => $tech]);

        return $pdf->download($tech->title.'.pdf');
    }

    public function surveyForm()
    {
        return view('pages.surveyForm');
    }

    public function industryProfileView($industry_id)
    {
        return view('pages.industryProfile');
    }
}
