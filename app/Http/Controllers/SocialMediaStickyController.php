<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SocialMediaSticky;

class SocialMediaStickyController extends Controller
{
    //
    public function add(Request $request)
    {
        $this->validate($request, array(
            'bgcolor' => 'required',
            'name' => 'required',
            'description' => 'required',
            'link' => 'required',
            'thumbnail' => 'required',
        ));

        $social = new SocialMediaSticky();

        if ($request->hasFile('thumbnail')) {
            $filename = time().'.'.$request->thumbnail->getClientOriginalExtension();
            request()->thumbnail->move(public_path('/storage/page_images/'), $filename);
            $social->thumbnail = $filename;
        }

        $social->name = $request->name;
        $social->link = $request->link;
        $social->description = $request->description;
        $social->bgcolor = $request->bgcolor;
        $social->save();

        return redirect()->back()->with('success', 'Social Media Icon Added.');
    }

    public function edit(Request $request, $social_id)
    {
        $this->validate($request, array(
            'name' => 'required',
            'description' => 'required',
            'link' => 'required',
        ));

        $social = SocialMediaSticky::find($social_id);

        if($social == null) {
            return redirect('/admin/manageLandingPage')->with('error', 'Social Media Sticky item not found.');
        }

        if ($request->hasFile('thumbnail')) {
            if ($social->thumbnail != null) {
                $image_path = public_path().'/storage/page_images/'.$social->thumbnail;

                if (file_exists($image_path)) {
                    unlink($image_path);
                }
            }

            $filename = time().'.'.$request->thumbnail->getClientOriginalExtension();
            request()->thumbnail->move(public_path('/storage/page_images/'), $filename);
            $social->thumbnail = $filename;
        }

        $social->name = $request->name;
        $social->link = $request->link;
        $social->description = $request->description;
        $social->bgcolor = $request->bgcolor == null ? '#FFFFFF' : $request->bgcolor;
        $social->link = $request->link;
        $social->save();

        return redirect()->back()->with('success', 'Social Media Icon Updated.');
    }

    public function able($social_id)
    {
        $social = SocialMediaSticky::find($social_id);

        if($social == null) {
            return redirect('/admin/manageLandingPage')->with('error', 'Social Media Sticky item not found.');
        }
        
        $msg = 'Social Media Icon '.$social->name.': Enabled.';
        $social->disabled = abs($social->disabled - 1);

        if ($social->disabled == 1) {
            $msg = 'Social Media Icon '.$social->name.': Disabled.';
        }

        $social->save();

        return redirect()->back()->with('success', $msg);
    }

    public function delete($social_id)
    {
        $social = SocialMediaSticky::find($social_id);

        if ($social == null) {
            return redirect()->back()->with('error', 'Social Media Icon has already been deleted.');
        }

        if ($social->thumbnail != null) {
            $image_path = public_path().'/storage/page_images/'.$social->thumbnail;
            if (file_exists($image_path)) {
                unlink($image_path);
            }
        }

        $social->delete();

        return redirect()->back()->with('success', 'Social Media Icon Deleted.');
    }
}
