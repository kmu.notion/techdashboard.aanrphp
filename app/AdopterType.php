<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Victorlap\Approvable\Approvable;

class AdopterType extends Model
{
    use Approvable;
    
    public function adopters()
    {
        return $this->hasMany(Adopter::class);
    }

    public function potentialAdopters()
    {
        return $this->hasMany(PotentialAdopter::class);
    }
}
