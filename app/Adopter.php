<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Victorlap\Approvable\Approvable;

class Adopter extends Model
{
    use Approvable;
    
    public function adopter_type()
    {
        return $this->belongsTo(AdopterType::class);
    }

    public function technologies()
    {
        return $this->belongsToMany(Technology::class);
    }
}
